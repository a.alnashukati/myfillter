//
//  FeedsViewModel.swift
//  MyFillter
//
//  Created by Ahmad on 12/14/19.
//  Copyright © 2019 Ahmad. All rights reserved.
//

import Foundation
import Firebase

class FeedsViewModel {
    
  private   var feeds = [Feeds]()
    
    init() {

       setData()
        
    }
    
    
    func getData( with completion: @escaping ([Feeds])->()){
        completion(self.feeds)
    }
    func getFeeds()->[Feeds]{
        return self.feeds
    }
    
    
    func setData(){
        AuthService.inctance.getFeeds { (feeds) in
            self.feeds = feeds
            
               }
        
    }
}
