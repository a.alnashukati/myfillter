//
//  GifHelper.swift
//  MyFillter
//
//  Created by Ahmad on 7/7/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import  AVFoundation

class Gif_helper {
    
    

    
    
    
  class   func addWaterMark(videoUrl: URL , with effects : [EffectsObject],completion: @escaping (_ error: Error?, _ url: URL?) -> Void){
        

        let videoAsset = AVAsset(url: videoUrl)
    
     let mixComposition = AVMutableComposition()
    
    
        let videoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
    
        let assetVideoTrack = videoAsset.tracks(withMediaType: .video)[0]
   
   if  let assetAudioTrak = videoAsset.tracks(withMediaType: .audio).first {
    let audioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)

        do {
            
            if let audioTraclInVideo = audioTrack {
                
                try audioTraclInVideo.insertTimeRange(CMTimeRangeMake(kCMTimeZero, (assetAudioTrak.asset?.duration)!), of: assetAudioTrak, at: kCMTimeZero)
            }
        }catch {
            
        }
    }
    
    
    
    
       let endTime  = assetVideoTrack.asset?.duration
        do{
            try  videoTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, (assetVideoTrack.asset?.duration)!), of: assetVideoTrack, at: kCMTimeZero)
            
           
        
        }catch{
            
        }
    
    
    
        videoTrack?.preferredTransform = assetVideoTrack.preferredTransform
        let videoSize = videoTrack?.naturalSize
        
        let parentLayer = CALayer()
        let videoLayer = CALayer()
        parentLayer.frame = CGRect(x: 0, y: 0, width: (videoSize?.width)!, height: (videoSize?.height)!)
            print(parentLayer.frame)
        videoLayer.frame = CGRect(x: 0, y: 0, width: (videoSize?.width)!, height: (videoSize?.height)!)
        parentLayer.addSublayer(videoLayer)
        print(effects.count)
    
    let titleLayer = CATextLayer()
    // titleLayer.backgroundColor = UIColor.white.cgColor
    titleLayer.string = "هذا نص تجريبي "
    titleLayer.font = UIFont.getBen(size: 25)
    titleLayer.shadowOpacity = 0
    titleLayer.alignmentMode = kCAAlignmentCenter
    titleLayer.frame = CGRect(x: 50, y: 100, width: (videoSize?.width)!-100, height: 70)
    titleLayer.beginTime = 4
    parentLayer.addSublayer(titleLayer)
    titleLayer.foregroundColor = UIColor.red.cgColor
    
    
    let textAnimation = CAKeyframeAnimation(keyPath: "transform.scale.x")
    //CABasicAnimation(keyPath: "transform.scale")
  //  textAnimation.fromValue =  1.3
  //  textAnimation.toValue = 0
   // textAnimation.values = [0 , 1.5 , 1 , 0 ]
  //  textAnimation.keyTimes = [0 , 0.2  , 1 , 1]
    
    textAnimation.values = [0 , 1.29 , 1.15 , 1.1 ,1.2 ,0.5, 1 ]
    textAnimation.keyTimes = [0 , 0.3  , 0.35 , 0.35 ,0.3, 0.3,0.5]

    textAnimation.beginTime = AVCoreAnimationBeginTimeAtZero
    textAnimation.repeatCount = 1
    textAnimation.duration = 1
    
    
    let textAnimation2 = CABasicAnimation(keyPath: "transform.scale")
    textAnimation2.fromValue =  0// (titleLayer.bounds.size.width) * 3
    textAnimation2.toValue =   1.2 //titleLayer.bounds.size.width
    textAnimation2.beginTime = AVCoreAnimationBeginTimeAtZero
    textAnimation2.repeatCount = 1.2
    textAnimation2.duration = 1
    
    textAnimation.isRemovedOnCompletion = false
   // titleLayer.add(textAnimation2, forKey: "bounds")
   
    
    titleLayer.add(textAnimation, forKey: nil)


        
  
    let objectiveCHelper = GifHelper()

        
        
        for effect in effects {
            
                let layer = CALayer()
                let animation = objectiveCHelper.animationForGif(with: effect.url)
                let fram = VideoInfoHelper.sheard.videoSizeConverter(LayerInVideo: parentLayer.frame, gifLayer: effect.LayerFramInWorkspace!)
                layer.frame = fram
                let begain :CFTimeInterval = effect.BeginTime!
                print(begain)
                if begain < 1 {
                    animation?.beginTime = AVCoreAnimationBeginTimeAtZero
                    
                }else{
                    animation?.beginTime = begain
                    
                }
                
                
                
                animation?.isRemovedOnCompletion = false
                layer.drawsAsynchronously = true
                layer.add(animation!, forKey: "gif")
                print(layer.frame)
                parentLayer.addSublayer(layer)
            
          
            
        }
        
        let videoComp = AVMutableVideoComposition()
        videoComp.renderSize = videoSize!
        parentLayer.isGeometryFlipped  = false 
        videoComp.frameDuration = CMTimeMake(1, 30)
        videoComp.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
        let insatraction = AVMutableVideoCompositionInstruction()
        insatraction.timeRange = CMTimeRangeMake(kCMTimeZero, endTime!)
        let layerInstraction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack!)
        insatraction.layerInstructions = [layerInstraction]
        videoComp.instructions = [insatraction]
        
        let savePathUrl: URL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/13.mp4")
        do { // delete old video
            try FileManager.default.removeItem(at: savePathUrl)
        } catch { print(error.localizedDescription) }
        
        let exporter:AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        exporter.outputURL = savePathUrl
        exporter.outputFileType = .mp4
        exporter.shouldOptimizeForNetworkUse  = true
        exporter.videoComposition = videoComp
        exporter.exportAsynchronously(completionHandler: {
            switch exporter.status {
            case AVAssetExportSessionStatus.completed:
                print("success:\(savePathUrl)")
                completion(nil , savePathUrl )
            case AVAssetExportSessionStatus.failed:
                completion(exporter.error, nil)
            case AVAssetExportSessionStatus.cancelled:
                print("cancelled \(exporter.error?.localizedDescription ?? "error nil")")
                completion(exporter.error, nil)

            default:
                print("complete")
                completion(exporter.error, nil)

            }
        })
        
        
    }
    
    
    //MARK: Text Animation
    
    /*     let titleLayer = CATextLayer()
      titleLayer.backgroundColor = UIColor.white.cgColor
     titleLayer.string = "Dummy text"
      titleLayer.font = UIFont(name: "Helvetica", size: 28)
     titleLayer.shadowOpacity = 0.5
     titleLayer.alignmentMode = kCAAlignmentCenter
     titleLayer.frame = CGRect(x: 0, y: 50, width: (videoSize?.width)!, height: (videoSize?.height)! / 6)
     parentLayer.addSublayer(titleLayer)
     
     let textAnimation = CABasicAnimation(keyPath: "transform.scale")
     textAnimation.fromValue = 0.2
     textAnimation.toValue = 1
     textAnimation.beginTime = 5
     textAnimation.repeatCount = 3
     textAnimation.duration = 2
     textAnimation.isRemovedOnCompletion = false
     titleLayer.add(textAnimation, forKey: nil)
     
     
     
     let gifLayer = CALayer()
     gifLayer.frame = CGRect(x: 0, y: 0, width: 500, height: 500)
     var  gifLayerAnimation = CAKeyframeAnimation()
     let url = Bundle.main.url(forResource: "3", withExtension: "gif")
     
     
     gifLayerAnimation =  objectiveCHelper.animationForGif(with: url)
     
     
     
     gifLayerAnimation.beginTime = 1
     
     gifLayer.drawsAsynchronously = true
     gifLayerAnimation.isRemovedOnCompletion = false
     gifLayer.add(gifLayerAnimation, forKey: "gif")
     
     parentLayer.addSublayer(gifLayer)*/
    
    //MARK: lift right
    /*
     let textAnimation = CABasicAnimation(keyPath: "bounds.size.width")
     textAnimation.fromValue =  (titleLayer.bounds.size.width) * 3
     textAnimation.toValue = titleLayer.bounds.size.width
     textAnimation.beginTime = AVCoreAnimationBeginTimeAtZero
     textAnimation.repeatCount = 1
     textAnimation.duration = 0.3
     
     textAnimation.isRemovedOnCompletion = false
     titleLayer.add(textAnimation, forKey: "bounds")
     */
    
}
