//
//  VideoPlayr.swift
//  MyFillter
//
//  Created by Ahmad on 5/28/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import AVFoundation
import FLAnimatedImage
class VideoPlayer : UIView {
  
    var videoURL : URL!
    var scaleValue : CGFloat = 1
    var controller :CameraController?
    var isPlaying = false 
    var player : AVPlayer?
    var isFinshPlaying =  false
    var controllerReferanc: workingSpaceVc?
    

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupPlayer()

    }
    
    

     init(url:URL , frame : CGRect){
        super.init(frame: frame)
        self.videoURL = url
        self.layer.borderColor = UIColor.blue.cgColor
        setupPlayer()

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
  
    var  palerlayer : AVPlayerLayer?
    
    func setupPlayer(){
        
        if let url = videoURL {
             player = AVPlayer(url: url)
             palerlayer = AVPlayerLayer(player: player)
            self.layer.addSublayer(palerlayer!)
            palerlayer?.frame = self.bounds
            
            player?.addObserver(self, forKeyPath:#keyPath(AVPlayerItem.status), options: .new, context: nil)
            let interval = CMTime(value: 1, timescale: 5)
            
            NotificationCenter.default.addObserver(self, selector: #selector(onPlayerFinsh(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
           
            let videoQue = DispatchQueue(label: "VidQi")
            player?.addPeriodicTimeObserver(forInterval: interval, queue: videoQue, using: { (progressTime) in
                let secounds  = CMTimeGetSeconds(progressTime)
                let secoundText = String(format: "%02d",Int( secounds ) % 60 )
                let minutesText = String(format: "%02d", Int(secounds) / 60 )
                DispatchQueue.main.async {
                                    self.controllerReferanc?.startScounds.text = "\(minutesText):\(secoundText)"

                }
                
                
                if let duration = self.player?.currentItem?.duration {
                    let durationSecound = CMTimeGetSeconds(duration)
                    let full = (durationSecound - secounds)
                    let x:CGFloat = CGFloat( secounds ) * self.scaleValue
                    
                    self.controllerReferanc?.scrollColletion(to: x, PointY: 0)
                 
                    if full > 0 {
                    let reaminSecoundText = String(format: "%02d",Int( full ) % 60 )
                    let reaminMinutesText = String(format: "%02d", Int(full) / 60 )
                        DispatchQueue.main.async {
                              self.controllerReferanc?.remaingScounds.text = "\(reaminMinutesText):\(reaminSecoundText)"
                        }
                  
                    }else{
                        
                    }
                }
            })
            
            
            
            
            
            let timeinterval = CMTime(seconds: 1, preferredTimescale: 1)
            let soundsQueue = DispatchQueue(label: "Sound")
            let effictsQueue = DispatchQueue(label: "Effect")
            let TextQueue = DispatchQueue(label: "Text")

        
            player?.addPeriodicTimeObserver(forInterval: timeinterval, queue: effictsQueue, using: { (Time) in
                
                if self.isFinshPlaying {
        
                }else {
                
                
                
                let currentSecound = Int(Time.seconds)
                let effects = self.controllerReferanc?.effectArray
              
                for effect in effects! {
                    let itmeQue = DispatchQueue(label: "item")
                    itmeQue.async {
                        if effect.statPoint == currentSecound {
                            
                            DispatchQueue.main.async {
                                
                                let  view = FLAnimatedImageView()
                                view.contentMode = .scaleAspectFit
                                view.frame = effect.LayerFramInWorkspace!
                                
                                let path = effect.url
                                do {
                                    let data = try Data(contentsOf: path!)
                                    let gifimage = FLAnimatedImage(animatedGIFData: data)
                                    view.animatedImage = gifimage
                                    
                                }catch{
                                    
                                }
                                
                                self.controllerReferanc?.layerView.addSubview(view)
                                view.loopCompletionBlock = {
                                    _ in
                                    view.removeFromSuperview()
                                }
                                
                                
                                
                                
                            }
                            
                            
                            
                            
                        }else {
                            
                        }

                    }
                    
                    
                    
                    
                    
                }
                
               
                
                }
           
            })
            
            
            player?.addPeriodicTimeObserver(forInterval: timeinterval, queue: soundsQueue, using: { (time) in
                
                let currentSecond = ( time.seconds)
        
                let sounds = self.controllerReferanc?.soundEffectsArray
                
                
                if self.isFinshPlaying {
                    
                    for sound in sounds! {
                        if Int ((sound.soundItem?.startPoint)! ) == Int (currentSecond) {
                            print()
                            sound.setupSoundPlayer()
                            sound.soundPlayer?.pause()
                            
                            
                        }
                        
                        
                        
                        
                    }
                    
                    
                }else {
                    
                    
                    
                    for sound in sounds! {
                        if Int ((sound.soundItem?.startPoint)! ) == Int (currentSecond) {
                            print()
                            sound.setupSoundPlayer()
                            sound.soundPlayer?.play()
                            
                            
                        }
                        
                        
                        
                        
                    }
                    
                }
                
       
                
                
            })
            
     
            player?.addPeriodicTimeObserver(forInterval: timeinterval, queue: TextQueue, using: { (time) in
                
                
            })
        }
        
       
        
        
    }
    
    
    func stop(count : Int ){
        
        
    }
    
    @objc func onPlayerFinsh(note :NSNotification){
        
        controllerReferanc?.palyBtn.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(700)) {
            self.player?.seek(to: CMTime(seconds: -1, preferredTimescale: 1))

        }

        isPlaying = false
        isFinshPlaying = true
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == #keyPath(AVPlayerItem.status){
            
            

        }
    }
    
    
    
    @objc func onOffBtnFunction(){
        let playButnonRef = controllerReferanc?.palyBtn
        if isFinshPlaying {


            isFinshPlaying = false
            playButnonRef?.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            isPlaying = true

              player?.play()
        }else {
            if isPlaying {
                
                player?.pause()
                isPlaying = false
                playButnonRef?.setImage(#imageLiteral(resourceName: "play"), for: .normal)
                
                
                pauseAllAoudio()

            }else {
                player?.play()
                isPlaying = true
                playButnonRef?.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                 palyAllAoudio()
            }
            
        }
        
        
       
        
        
    
    }
    
    
   func  pauseAllAoudio(){
    
    for  i in (self.controllerReferanc?.soundEffectsArray)! {
        i.soundPlayer?.pause()
    }
    
    
    }
    
    
    func palyAllAoudio(){
        for  i in (self.controllerReferanc?.soundEffectsArray)! {
            i.soundPlayer?.play()
        }
        
    }
    
    
    
   
    }


