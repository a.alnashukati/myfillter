//
//  ImageEffectsHelper.swift
//  MyFillter
//
//  Created by Ahmad on 6/25/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
class ImageEffectsHelper {
    
    
    
    class func addImageToVideo(videoUrl: URL, Image: UIImage){
        
        let composition = AVMutableComposition()
        let videoAsset = AVAsset(url: videoUrl)
        
        /// Tracking Video
        let video_Tracks = videoAsset.tracks(withMediaType: .video)
        let videoTrack  = video_Tracks[0]
        let duration = videoTrack.timeRange.duration
        let vid_timerange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration)

        let compositionVideoTrack = composition.addMutableTrack(withMediaType: .video, preferredTrackID: CMPersistentTrackID())
        do {
        try compositionVideoTrack?.insertTimeRange(vid_timerange, of: videoTrack, at: kCMTimeZero)
        }catch{
            print("error")
        }
        
        compositionVideoTrack?.preferredTransform = videoTrack.preferredTransform
        
        
        
        /// Image Effects
        let size = videoTrack.naturalSize

        
        let imageLayer = CALayer()
        imageLayer.contents = Image.cgImage
        imageLayer.frame = CGRect(x: 5, y: 5, width: 100, height: 100)
        
        //Text Effects
        let titleLayer = CATextLayer()
       // titleLayer.backgroundColor = UIColor.white.cgColor
        titleLayer.string = "Dummy text"
       // titleLayer.font = UIFont(name: "Helvetica", size: 28)
        titleLayer.shadowOpacity = 0.5
        titleLayer.alignmentMode = kCAAlignmentCenter
        titleLayer.frame = CGRect(x: 0, y: 50, width: size.width, height: size.height / 6)
        
        
     let videoLayer = CALayer()
      videoLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        let parentLayer = CALayer()
        parentLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        parentLayer.addSublayer(videoLayer)
        parentLayer.addSublayer(imageLayer)
        parentLayer.addSublayer(titleLayer)
        
        
        let layersComposition = AVMutableVideoComposition()
        
        layersComposition.frameDuration = CMTimeMake(1, 30)
        layersComposition.renderSize = size
        layersComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
        
        // instruction for watermark
     
         let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, composition.duration)
        let video_track = composition.tracks(withMediaType: .video)[0]
         let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: video_track)
        instruction.layerInstructions = [layerInstruction]
        layersComposition.instructions = [instruction]

        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docsDir = dirPaths[0] as String
        let movieFilePath = docsDir.appending("result.mp4")
        
        let movieDestinationUrl = URL(fileURLWithPath: movieFilePath)
        //remove existing file
        _ = try? FileManager().removeItem(at: movieDestinationUrl)
        
        
        // use AVAssetExportSession to export video
        guard let assetExport = AVAssetExportSession(asset: composition, presetName:AVAssetExportPresetHighestQuality) else {return}
        assetExport.videoComposition = layersComposition
        assetExport.outputFileType = .mp4
        assetExport.outputURL = movieDestinationUrl
        assetExport.exportAsynchronously(completionHandler: {
            switch assetExport.status{
            case  AVAssetExportSessionStatus.failed:
                print("failed \(assetExport.error)")
            case AVAssetExportSessionStatus.cancelled:
                print("cancelled \(assetExport.error)")
            case AVAssetExportSessionStatus.completed:
                print("Completed")
            default:
                print("unknown")
            }
        })
    }
    
    
    
}
