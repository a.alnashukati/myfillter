//
//  VideoHelper.swift
//  MyFillter
//
//  Created by Ahmad on 6/27/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import AVFoundation
import UIKit
class VideoHelper {
    
    
    class func mergeVideos(firestUrl : URL , SecondUrl : URL ){
        
        let firstAssets = AVAsset(url: firestUrl)
        let secondAssets = AVAsset(url: SecondUrl)
        print(firestUrl)
        print(SecondUrl)
        let mixComposition = AVMutableComposition()
        
        let firstTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        let seconTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)

        do{
      try   firstTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, firstAssets.duration), of: firstAssets.tracks(withMediaType: .video)[0], at: kCMTimeZero)
        try seconTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, secondAssets.duration), of: secondAssets.tracks(withMediaType: .video)[0], at: kCMTimeZero)
        
        
        }catch{
           
            
        }
        
        let mainInstraction = AVMutableVideoCompositionInstruction()
    
        mainInstraction.timeRange  = CMTimeRangeMake(kCMTimeZero, firstAssets.duration)
        
        let firstLayerInstraction = AVMutableVideoCompositionLayerInstruction(assetTrack: firstTrack!)
        let scale = CGAffineTransform(scaleX: 0.6, y: 0.6)
        let move = CGAffineTransform(translationX:320, y: 320)
        let transform = scale.concatenating(move)
        firstLayerInstraction.setTransform(transform, at: kCMTimeZero)
        
        
        let secondLayerInstraction = AVMutableVideoCompositionLayerInstruction(assetTrack: seconTrack!)
        let secondScale = CGAffineTransform(scaleX: 0.9, y: 0.9)
        let secondMove = CGAffineTransform(translationX: 0, y: 0)
        let secondTransform = secondScale.concatenating(secondMove)
        secondLayerInstraction.setTransform(secondTransform, at: kCMTimeZero)
      
        var layerArray = [AVVideoCompositionLayerInstruction]()
        layerArray.append(firstLayerInstraction)
        layerArray.append(secondLayerInstraction)
        mainInstraction.layerInstructions = [firstLayerInstraction , secondLayerInstraction]
        
     let  mainCompositonInst = AVMutableVideoComposition()
        mainCompositonInst.instructions = [mainInstraction]
        mainCompositonInst.frameDuration = CMTimeMake(1, 30)
        mainCompositonInst.renderSize = CGSize(width: 640, height: 480)
        
        
      let saveUrl = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/44.mov")
      
       
        
       
        
       
       guard let assetExport = AVAssetExportSession(asset: mixComposition, presetName:AVAssetExportPresetHighestQuality) else {return}
        assetExport.videoComposition = mainCompositonInst
        assetExport.outputFileType = .mov
        assetExport.outputURL = saveUrl
        assetExport.exportAsynchronously(completionHandler: {
            switch assetExport.status{
            case  AVAssetExportSessionStatus.failed:
                print("failed \(assetExport.error)")
            case AVAssetExportSessionStatus.cancelled:
                print("cancelled \(assetExport.error)")
            case AVAssetExportSessionStatus.completed:
                print("Completed\(saveUrl)")
            default:
                print("unknown")
            }
        })
        
    }
    
    
    
    
    
    
    
    
}
