//
//  CamerController .swift
//  MyFillter
//
//  Created by Ahmad on 5/31/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import AVFoundation
import UIKit
class CameraController : NSObject {
    
    var controller : UIViewController?

    public enum CameraPosition {
        case front
        case rear
    }
    var CaptureSession: AVCaptureSession?
    var FrontCamera : AVCaptureDevice?
    var rearCamera : AVCaptureDevice?
    var currentCameraPosition:CameraPosition?
    var frontCameraInput : AVCaptureDeviceInput?
    var  rearCameraInput : AVCaptureDeviceInput?
    
    var PhotoOutput : AVCapturePhotoOutput?
    
    var previewLayer : AVCaptureVideoPreviewLayer?
    var flashMode = AVCaptureDevice.FlashMode.off
    
    var outputURL : URL!
    var photoCaptureCompletionBlock  :((UIImage? , Error?)->Void)?

func prepare(completionHandler: @escaping (Error?) -> Void) {
    
    func CreateCptureSession() {
        self.CaptureSession = AVCaptureSession()
    }
   
    func configureCaptureDevices() throws {
        
        
        let session   = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .unspecified)
        let cameras =  session.devices
        if cameras.isEmpty {throw CameraControllerError.noCamerasAvailable}
        
        for camera in cameras {
            if camera.position  == .front {
                self.FrontCamera = camera
            }
            if camera.position == .back {
                self.rearCamera = camera
                try camera.lockForConfiguration()
                camera.focusMode  = .continuousAutoFocus
                camera.unlockForConfiguration()
            }
        }

    }
   
    func configurDeviceInput() throws {
    
        guard let captureSession = self.CaptureSession else {throw CameraControllerError.captureSessionIsMissing}
        if let rearCamera = self.rearCamera {
            self.rearCameraInput = try AVCaptureDeviceInput(device: rearCamera)
          
            if captureSession.canAddInput(self.rearCameraInput!) {
                captureSession.addInput(self.rearCameraInput!)
                activeInput = self.rearCameraInput
                }
            
            self.currentCameraPosition = .rear
            
        }else if let frontCamer = self.FrontCamera {
            
            self.frontCameraInput = try AVCaptureDeviceInput(device:frontCamer)
            if captureSession.canAddInput(self.frontCameraInput!){
                captureSession.addInput(self.frontCameraInput!)
                activeInput = self.frontCameraInput
            }
            self.currentCameraPosition = .front
        }
        else {
            throw CameraControllerError.noCamerasAvailable
        }
        
    }
    func configurePhotoOutput() throws {
        
        
        guard let captureSession = self.CaptureSession else {throw CameraControllerError.captureSessionIsMissing
        }
        self.PhotoOutput = AVCapturePhotoOutput()
        self.PhotoOutput!.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecType.jpeg])], completionHandler: nil)

        if captureSession.canAddOutput(self.PhotoOutput!) {
            captureSession.addOutput(self.PhotoOutput!)
            captureSession.startRunning()
        }
    }
    
    
    DispatchQueue(label: "prepare").async {
        do {
            CreateCptureSession()
        try configureCaptureDevices()
        try configurDeviceInput()
         try configurePhotoOutput()
            try self.MovieCaputr()
            
        }
        catch {
            DispatchQueue.main.async {
                completionHandler(error)
            }
            return
        }
        
        DispatchQueue.main.async {
            completionHandler(nil)
        }
        
    }
    
    }
    
    
    func displayPreview(on view : UIView) throws {
        
        guard let captureSession = self.CaptureSession, captureSession.isRunning else { throw CameraControllerError.captureSessionIsMissing }
        self.previewLayer  = AVCaptureVideoPreviewLayer(session: captureSession)
         self.previewLayer?.videoGravity = .resizeAspectFill
        self.previewLayer?.connection?.videoOrientation = .portrait
        
        view.layer.insertSublayer(self.previewLayer!, at: 0)
        
        self.previewLayer?.frame = view.frame
        
    }
    
    
    func switchCameras() throws {
        
        guard let currentCameraPosition = currentCameraPosition , let capSession = self.CaptureSession , capSession.isRunning   else  { throw  CameraControllerError.captureSessionIsMissing }
        capSession.beginConfiguration()
        
        func switchToFrontCamera() throws {}
        func switchToRearCamera() throws {}
        
        switch currentCameraPosition {
        case .front :
            try switchToFrontCamera()
        case .rear :
            
            try switchToRearCamera()
        }
     capSession.commitConfiguration()
    }
    
    func captureImage(completion:@escaping (UIImage? , Error?)->Void){
        guard let captureSession =  CaptureSession , captureSession.isRunning else { completion(nil , CameraControllerError.captureSessionIsMissing); return        }
        
        let sitting = AVCapturePhotoSettings()
        sitting.flashMode = self.flashMode
        self.PhotoOutput?.capturePhoto(with: sitting, delegate: self)
        self.photoCaptureCompletionBlock = completion
    }
    
    
    /////////////// -- stackOverFlow---////////////////////////
    var moveOutput = AVCaptureMovieFileOutput()
    var activeInput: AVCaptureDeviceInput!

    func MovieCaputr() throws {
        
        guard let microphone = AVCaptureDevice.default(for: .audio) else { throw CameraControllerError.unknown}
        guard let capturesession  = self.CaptureSession else { throw CameraControllerError.captureSessionIsMissing}
        
            let micInput = try AVCaptureDeviceInput(device: microphone)
            if capturesession.canAddInput(micInput){
                capturesession.addInput(micInput)
            }
        
       let moveOutput = self.moveOutput
            if capturesession.canAddOutput(moveOutput) {
                capturesession.addOutput(moveOutput)
            }
        

            
        }
        
    func startSession() throws{
        guard let capturesession  = self.CaptureSession else { throw CameraControllerError.captureSessionIsMissing}
        
        if !capturesession.isRunning {
            videoQueue().async {
                capturesession.startRunning()
            }
        }

    }
    
    
    func stopSession() throws {
        guard let capturesession  = self.CaptureSession else { throw CameraControllerError.captureSessionIsMissing}
        
        if capturesession.isRunning {
            videoQueue().async {
                capturesession.stopRunning()
            }
        }

    }
    
    
    func videoQueue() -> DispatchQueue {
        return DispatchQueue.main
    }

        
  
    func currentVideoOreantation()->AVCaptureVideoOrientation {
        var oreintation : AVCaptureVideoOrientation
        switch UIDevice.current.orientation {
        case .portrait:
            oreintation = .portrait
        case .landscapeLeft :
            oreintation = .landscapeLeft
        case .landscapeRight :
            oreintation = .landscapeRight
        case .portraitUpsideDown :
            oreintation = .portraitUpsideDown
        default:
            oreintation = .landscapeRight
        }
        return oreintation
    }
    
    func tempURL()->URL?{
        let directory = NSTemporaryDirectory() as NSString
        if directory != "" {
            let path = directory.appendingPathComponent(UUID().uuidString + ".mp4")
            return URL(fileURLWithPath: path)
        }
        return nil
    }
    
    
    func startRecording(){
        
        let  moveOutput = self.moveOutput
            if moveOutput.isRecording  == false {
            let connection = moveOutput.connection(with: .video)
                if (connection?.isVideoOrientationSupported)! {
                    connection?.videoOrientation = currentVideoOreantation()
                }
                
                if (connection?.isVideoStabilizationSupported)! {
                    connection?.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
                }
            let device = activeInput.device
                if device.isSmoothAutoFocusSupported {
                    do {
                        try device.lockForConfiguration()
                        device.isSmoothAutoFocusEnabled = false
                        
                        device.unlockForConfiguration()
                    }
                    catch {
                        print("Error setting configuration: \(error)")

                    }
                }
             outputURL = tempURL()
                moveOutput.startRecording(to: outputURL, recordingDelegate: self)
            }else {
                stopRecording()

            }
        
        
        
    }
    
    func startCapture(){
        startRecording()

    }
    
    
    func stopRecording(){
        let moveOutput = self.moveOutput
            if moveOutput.isRecording {
                moveOutput.stopRecording()
            }
            
        
    }

    
    enum CameraControllerError : Swift.Error {
        case captureSessionAlreadyRunning
        case captureSessionIsMissing
        case inputsAreInvalid
        case invalidOperation
        case noCamerasAvailable
        case unknown
    }
}



extension CameraController:AVCapturePhotoCaptureDelegate ,AVCaptureFileOutputRecordingDelegate {
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        print("Staring")
        //output.maxRecordedDuration = CMTime(seconds: 10, preferredTimescale: 1)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(9)) {
            print(123)
            output.stopRecording()
        }
        
        return

    }
  
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if error != nil {
            print("Error")
        }else {
            UISaveVideoAtPathToSavedPhotosAlbum(outputURL.path, nil, nil, nil)
            _ = outputURL as URL
            
            let workView = workingSpaceVc()
          workView.url  = outputURL
            self.controller?.navigationController?.pushViewController(workView , animated: true )
            

        }
    }
    
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let error = error {self.photoCaptureCompletionBlock?(nil ,error)      }
        
        else if let imageData = photo.fileDataRepresentation() , let Image = UIImage(data: imageData) {
            
            self.photoCaptureCompletionBlock?(Image , nil)
        }else {
            self.photoCaptureCompletionBlock?(nil , CameraControllerError.unknown)
        }
    }
    
    func getUrl()->URL {
        print(outputURL)
        return outputURL
    }
    
}





