//
//  ImageHelper.swift
//  MyFillter
//
//  Created by Ahmad on 6/4/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import AVFoundation
import UIKit
class imagesHelper{
    
    
    class func sharedInstance(images:[UIImage]){
        let image_helper = imagesHelper()
          image_helper.imageToVideo(Images: images)
    }
    
   func imageToVideo(Images:[UIImage]){
        let outputSize = CGSize(width: 1920, height: 1280)

        let imagesPerSecounds : TimeInterval = 3
        var selectedPhotoArray = Images
        var imageArrayToVideoUrl : URL!
        let audioIsEnable :Bool = false
        var asset : AVAsset?
    
        func buikdVideoFromImageArray(){
           print(selectedPhotoArray)
            imageArrayToVideoUrl = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/video1.mov")
            print(imageArrayToVideoUrl)
           removeFileAtURLIfExists(url: imageArrayToVideoUrl)
          guard let videoWriter = try? AVAssetWriter(outputURL: imageArrayToVideoUrl, fileType: AVFileType.mov)
            else {
                print("Assets Error")
                return
            }
            
            let compressionProperties : [String : AnyObject] = [
                AVVideoAverageBitRateKey : Int(1000000) as AnyObject,
                AVVideoMaxKeyFrameIntervalKey : Int(16) as AnyObject,
                AVVideoProfileLevelKey : AVVideoProfileLevelH264Main31 as AnyObject
            ]
            
            let outputSettings = [AVVideoCodecKey : AVVideoCodecType.h264 ,
                                  AVVideoWidthKey : Int( outputSize.width) ,
                                  AVVideoHeightKey : Int(outputSize.height) ,
                                  AVVideoCompressionPropertiesKey : compressionProperties as AnyObject] as [String:Any]
    
            guard  videoWriter.canApply(outputSettings: outputSettings, forMediaType: AVMediaType.video) else {
                print("cant Apply output")
                return
            }
            let videoWriteInput = AVAssetWriterInput(mediaType: .video, outputSettings: outputSettings)
            
            let sourcePixelBufferAttributesDictionary = [kCVPixelBufferPixelFormatTypeKey as String : Int( kCVPixelFormatType_32ABGR) , kCVPixelBufferWidthKey as String : Int(outputSize.width), kCVPixelBufferHeightKey as String : Float(outputSize.height)] as [String : Any]
            let pixelBufferAdappter = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: videoWriteInput, sourcePixelBufferAttributes: sourcePixelBufferAttributesDictionary)
            
            if videoWriter.canAdd(videoWriteInput){
                print(133)

                videoWriter.add(videoWriteInput)
            }
            if videoWriter.startWriting(){
                print(123)
                let zeroTime = CMTimeMake(Int64(imagesPerSecounds),Int32(1))
                videoWriter.startSession(atSourceTime: zeroTime)
                assert(pixelBufferAdappter.pixelBufferPool != nil)
                let media_queue = DispatchQueue(label: "mediaInputQueue")
                videoWriteInput.requestMediaDataWhenReady(on: media_queue) {
                    let fps :Int32 = 1
                    let framePerSecound = Int64(imagesPerSecounds)
                    let frameDuration = CMTimeMake(Int64(imagesPerSecounds), fps)
                    var frameCount : Int64 = 0
                    var appendSeccused = true
                    while(!selectedPhotoArray.isEmpty){
                        if videoWriteInput.isReadyForMoreMediaData {
                            let nextPhoto = selectedPhotoArray.remove(at: 0)
                            let lastFrameTime =  CMTimeMake(frameCount * framePerSecound, fps)
                            let presentationTime =  CMTimeAdd(lastFrameTime, frameDuration)
                            var pixelBuffer :CVPixelBuffer?  = nil
                            
                            let status:CVReturn  = CVPixelBufferPoolCreatePixelBuffer(kCFAllocatorDefault, pixelBufferAdappter.pixelBufferPool!, &pixelBuffer)
                            
                            if let pixelBuffer = pixelBuffer , status == 0 {
                                let managedPixelBuffer = pixelBuffer
                                CVPixelBufferLockBaseAddress(managedPixelBuffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
                                let data = CVPixelBufferGetBaseAddress(managedPixelBuffer)
                                let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
                                let context = CGContext(data: data, width: Int(outputSize.width), height: Int(outputSize.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(managedPixelBuffer), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
                                context?.clear(CGRect(x: 0, y: 0, width: outputSize.width, height: outputSize.height))
                                let horizontalRatio = outputSize.width / nextPhoto.size.width
                                let virticalRatio = outputSize.height / nextPhoto.size.height
                                let aspectRatio = min(horizontalRatio, virticalRatio)
                                let newSize = CGSize(width: nextPhoto.size.width * aspectRatio, height: nextPhoto.size.height * aspectRatio)
                                
                                let x = newSize.width < outputSize.width ? (outputSize.width - newSize.width)/2 : 0
                                let y = newSize.height < outputSize.height ? (outputSize.height - newSize.height) / 2 : 0
                                context?.draw(nextPhoto.cgImage!, in: CGRect(x: x, y: y, width: newSize.width, height: newSize.height))
                                CVPixelBufferUnlockBaseAddress(managedPixelBuffer, CVPixelBufferLockFlags(rawValue: CVOptionFlags(0)))
                                appendSeccused = pixelBufferAdappter.append(pixelBuffer, withPresentationTime: presentationTime)

                                
                            }else {
                                appendSeccused = false
                            }
                            if !appendSeccused {
                                break
                            }
                            videoWriteInput.markAsFinished()
                            videoWriter.finishWriting {
                                print("-----video1 url = \(imageArrayToVideoUrl)")
                                asset = AVAsset(url: imageArrayToVideoUrl)
                                

                            }
                            
                            
                            
                            
                        }
                        
                        
                    }
                }
            }

        }
    
    buikdVideoFromImageArray()
        
    }
        
    func removeFileAtURLIfExists(url: URL?) {
        
        
        if let filePath = url?.path {
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                do{
                    try fileManager.removeItem(atPath: filePath)
                } catch let error as NSError {
                    print("Couldn't remove existing destination file: \(error)")
                }
            }
        }
        }
    

        
        
        
    
    
    
    
}
