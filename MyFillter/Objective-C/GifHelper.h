//
//  GifHelperOC.h
//  MyFillter
//
//  Created by Ahmad on 7/12/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
@interface GifHelper : NSObject

- (CAKeyframeAnimation *) animationForGifWithURL : (NSURL *) url ;
- (CAKeyframeAnimation *)createGIFAnimation:(NSData *)data ; 
@end
