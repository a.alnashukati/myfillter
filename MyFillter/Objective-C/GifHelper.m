//
//  GifHelperOC.m
//  MyFillter
//
//  Created by Ahmad on 7/12/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GifHelper.h"
#import <AVFoundation/AVFoundation.h>
@implementation GifHelper



- (CAKeyframeAnimation *) animationForGifWithURL : (NSURL *) url {
    
    
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"contents"];
   
    NSMutableArray *frames = [NSMutableArray new ];
    NSMutableArray *delayTimes = [NSMutableArray new ];
    
    CGFloat totalTime = 0.0;
    CGFloat gifWidth;
    CGFloat gifHight;
    
    CGImageSourceRef gifSource = CGImageSourceCreateWithURL((CFURLRef)url, NULL) ;
    
    
    size_t frameCount = CGImageSourceGetCount(gifSource);
    for (size_t i  = 0 ; i < frameCount ; ++i){
        
        
        CGImageRef frame = CGImageSourceCreateImageAtIndex(gifSource, i, NULL);
        [frames addObject:(__bridge id)frame];
        CGImageRelease(frame);
        
        NSDictionary *dic = (NSDictionary *)CFBridgingRelease(CGImageSourceCopyPropertiesAtIndex(gifSource, i, NULL));
        
        gifWidth = [[dic valueForKey:(NSString * )kCGImagePropertyPixelWidth]floatValue];
        gifHight = [[dic valueForKey:(NSString *)kCGImagePropertyPixelHeight]floatValue];
        
        NSDictionary * gifDic = [dic valueForKey:(NSString * ) kCGImagePropertyGIFDictionary ];
        [delayTimes addObject:[gifDic valueForKey:(NSString *) kCGImagePropertyGIFUnclampedDelayTime]];
        totalTime = totalTime +  [[gifDic valueForKey:(NSString *)kCGImagePropertyGIFDelayTime]floatValue];
        
        

    }
    
    if (gifSource ){
        CFRelease(gifSource);
    }
    
    NSMutableArray * times = [NSMutableArray arrayWithCapacity:3];
    CGFloat currenttime = 0.0;
    NSInteger count = delayTimes.count ;
    for (int i = 0 ; i<count ; ++i) {
        [times addObject:[NSNumber numberWithFloat:(currenttime /totalTime)]];
        currenttime += [[delayTimes objectAtIndex:i]floatValue];
    }
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:3];
    for(int i = 0 ; i < count; ++ i){
        [images addObject:[frames objectAtIndex:i]];
    }
    
    animation.keyTimes = times;
    animation.values = images;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animation.duration = totalTime;
    animation.repeatCount = 1;
    
    
    
    return animation ;
}


- (CAKeyframeAnimation *)createGIFAnimation:(NSData *)data{
    
    CGImageSourceRef src = CGImageSourceCreateWithData((__bridge CFDataRef)(data), nil);
    int frameCount =(int) CGImageSourceGetCount(src);
    
    // Total loop time
    float time = 0;
    
    // Arrays
    NSMutableArray *framesArray = [NSMutableArray array];
    NSMutableArray *tempTimesArray = [NSMutableArray array];
    
    // Loop
    for (int i = 0; i < frameCount; i++){
        
        // Frame default duration
        float frameDuration = 0.1f;
        
        // Frame duration
        CFDictionaryRef cfFrameProperties = CGImageSourceCopyPropertiesAtIndex(src,i,nil);
        NSDictionary *frameProperties = (__bridge NSDictionary*)cfFrameProperties;
        NSDictionary *gifProperties = frameProperties[(NSString*)kCGImagePropertyGIFDictionary];
        
        // Use kCGImagePropertyGIFUnclampedDelayTime or kCGImagePropertyGIFDelayTime
        NSNumber *delayTimeUnclampedProp = gifProperties[(NSString*)kCGImagePropertyGIFUnclampedDelayTime];
        if(delayTimeUnclampedProp) {
            frameDuration = [delayTimeUnclampedProp floatValue];
        } else {
            NSNumber *delayTimeProp = gifProperties[(NSString*)kCGImagePropertyGIFDelayTime];
            if(delayTimeProp) {
                frameDuration = [delayTimeProp floatValue];
            }
        }
        
        // Make sure its not too small
        if (frameDuration < 0.011f){
            frameDuration = 0.100f;
        }
        
        [tempTimesArray addObject:[NSNumber numberWithFloat:frameDuration]];
        
        // Release
        CFRelease(cfFrameProperties);
        
        // Add frame to array of frames
        CGImageRef frame = CGImageSourceCreateImageAtIndex(src, i, nil);
        [framesArray addObject:(__bridge id)(frame)];
        
        // Compile total loop time
        time = time + frameDuration;
    }
    
    NSMutableArray *timesArray = [NSMutableArray array];
    float base = 0;
    for (NSNumber* duration in tempTimesArray){
        //duration = [NSNumber numberWithFloat:(duration.floatValue/time) + base];
        base = base + (duration.floatValue/time);
        [timesArray addObject:[NSNumber numberWithFloat:base]];
    }
    
    // Create animation
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"contents"];
    
    animation.duration = time;
    animation.repeatCount = 100;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    animation.values = framesArray;
    animation.keyTimes = timesArray;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animation.calculationMode = kCAAnimationDiscrete;
    
    return animation;
}








@end
