//
//  BtnCell.swift
//  MyFillter
//
//  Created by Ahmad on 5/24/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit

class btnCell : UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let imageBtn : UIImageView = {
        let im = UIImageView(image: #imageLiteral(resourceName: "loginIcone"))
        im.contentMode = .scaleAspectFit
        return im
    }()
    
    func setupView(){
        self.addSubview(imageBtn)
        _ = imageBtn.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
}







