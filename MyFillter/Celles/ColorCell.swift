//
//  ColorCell.swift
//  MyFillter
//
//  Created by Ahmad on 11/21/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
class ColorCell : UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let contaner : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        view.layer.cornerRadius = 25
        return view
        
    }()
    
    let color : UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        view.layer.cornerRadius = 20
        return view
    }()
    
    
    private func setupView(){
        
        self.addSubview(contaner)
        
        contaner.anchorToTop(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor)
        
        self.addSubview(color)
        _ = color.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        color.centerXAnchor.constraint(equalTo: contaner.centerXAnchor).isActive = true
        color.centerYAnchor.constraint(equalTo: contaner.centerYAnchor).isActive = true
    }
    
}
