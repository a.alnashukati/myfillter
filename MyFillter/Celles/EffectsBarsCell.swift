//
//  EffectsBarsCell.swift
//  MyFillter
//
//  Created by Ahmad on 7/16/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit

class EffectsBarsCell :UICollectionViewCell , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    var numberOfItems : Int = 1
   var scaleVule :CGFloat = 1
    var viewSize: CGFloat = 0
    var cell :effectsContaner?
    var workingInstance : workingSpaceVc?
    var  startPoint : CGFloat =  0
    
    lazy var effectsCollection : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.dataSource = self
        collection.delegate = self
        collection.backgroundColor = .clear
        layout.scrollDirection = .horizontal
        collection.showsHorizontalScrollIndicator = false
        return collection
        
    }()
    
    lazy var  imageBtn : UIImageView = {
        let im = UIImageView(image: #imageLiteral(resourceName: "loginIcone"))
        im.contentMode = .scaleAspectFit
        im.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return im
    }()
    
    let timeLine : UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        return view
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
         cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? effectsContaner
      
        return cell!
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let point = targetContentOffset.pointee.x
        let value = Double(point/scaleVule)
        self.workingInstance?.videoPlayer.player?.seek(to: CMTime(seconds: value, preferredTimescale: 1))
        self.workingInstance?.videoPlayer.isFinshPlaying = false 
    }
    
  
    
    
    
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
   
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        
        return UIEdgeInsets(top: 0, left: effectsCollection.frame.width / 2 - 45 , bottom: 0, right: effectsCollection.frame.width / 2 + 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: viewSize + 40 , height: self.frame.height - 20 )
    }
    
    
    
    func setupView(){
        self.addSubview(imageBtn)
        self.addSubview(timeLine)
        self.addSubview(effectsCollection)
        self.layer.cornerRadius = 5
        self.clipsToBounds = true 
        effectsCollection.register(effectsContaner.self , forCellWithReuseIdentifier: "cell")
        
        _ = imageBtn.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 0)
        _ = timeLine.anchor(self.topAnchor, left: imageBtn.rightAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = effectsCollection.anchor(self.topAnchor, left: imageBtn.rightAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
    }
    
   
}




