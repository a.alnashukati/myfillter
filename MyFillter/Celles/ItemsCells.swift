//
//  ItemsCell.swift
//  MyFillter
//
//  Created by Ahmad on 5/24/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
class ItemsCells : UICollectionViewCell{
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let item : UIImageView = {
        let im = UIImageView(image: #imageLiteral(resourceName: "plasecholder") .withRenderingMode(.alwaysTemplate))
        im.tintColor = .white 
        im.contentMode = .scaleAspectFit
        return im
    }()
    
    
    
     func setupView(){
        self.addSubview(item)
        _ = item.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        self.addSubview(item)
        self.layer.cornerRadius = 10
        self.backgroundColor = UIColor(white: 1, alpha: 0.5)
     
    }
    
    
}

    

class soundItemCells : ItemsCells {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let itemName : UILabel = {
        let lb = UILabel()
        lb.text = "Sound1.mp3"
        lb.textAlignment = .center
        lb.textColor = .white
        return lb
    }()
    
    override func setupView() {
        self.addSubview(item)
         self.addSubview(itemName)
        _ = item.anchor(self.topAnchor, left: nil , bottom: nil  , right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        item.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
       _ = itemName.anchor(item.bottomAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
          let image =   UIImage(named: "play_circle")?.withRenderingMode(.alwaysTemplate)
        item.image =  image
        item.tintColor = UIColor.rgb(red: 237,green: 123,blue: 81)
        self.backgroundColor = UIColor(white: 0, alpha: 0.5 )
        self.layer.cornerRadius = 10

        
    }
    
    
    
}
