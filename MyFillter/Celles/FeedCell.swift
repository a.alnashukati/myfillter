//
//  FeedCell.swift
//  MyFillter
//
//  Created by Ahmad on 12/14/19.
//  Copyright © 2019 Ahmad. All rights reserved.
//

import UIKit
class FeedsCell : UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let feedsImage : UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    
    lazy var  moreBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "baseline_more_vert_white_24pt_1x"), for: .normal)
        btn.tintColor = .white
        return btn
    }()
    
    
    func setupView(){
        self.addSubview(feedsImage)
        _ = feedsImage.anchor(self.topAnchor, left: self.leftAnchor, bottom:nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0  )
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
        
        let subview = UIView()
        subview.backgroundColor = UIColor(white: 0, alpha: 0.4)
        self.addSubview(subview)
        _ = subview.anchor(feedsImage.bottomAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30 )
        
        subview.addSubview(moreBtn)
        _ = moreBtn.anchor(subview.topAnchor, left: nil, bottom: subview.bottomAnchor, right: subview.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 30, heightConstant: 0)


    }
    
    
    
    
}
