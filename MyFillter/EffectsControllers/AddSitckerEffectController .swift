//
//  AddSitckerEffectController .swift
//  MyFillter
//
//  Created by Ahmad on 9/17/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
class  AddSitckerEffectController: AddEffectViewController {
    
    
    var url : URL?
    var gifRect: CGRect?
    let gifview = GifEffectView()
    let gifViewToviedo = GifEffectView()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    self.subview.backgroundColor = .clear
        workingSpaceView.backgroundColor = .green 
        navigationController?.navigationBar.barTintColor = .red 
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.subview.alpha = 1
            self.workingSpaceView.alpha = 1
            self.barView.alpha = 1

        }, completion: {(completed) in
            
            self.setupGifSubView(url: self.url!)
        })
        
      
        
        
    }

    let workingSpaceView = UIView()
    let barView = UIView()
    func setupView(){
        workingSpaceView.backgroundColor = .clear
        
    
        
     topContanert.addSubview(workingSpaceView)
        workingSpaceView.frame = (workInctance?.layerView.frame)!
        
        
        barView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        topContanert.addSubview(barView)
        _ = barView.anchor(topContanert.topAnchor, left: topContanert.leftAnchor, bottom: workingSpaceView.topAnchor, right: topContanert.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        
        

        self.workingSpaceView.alpha = 0
        self.barView.alpha = 0

    }
    
    func setupGifSubView(url : URL ){
        gifview.setGifForView(Url: url)
        gifViewToviedo.setGifForView(Url: url )
        workingSpaceView.addSubview(gifview)
    
        gifview.frame = CGRect(x: workingSpaceView.frame.width / 2  - 50 , y: workingSpaceView.frame.height/2  - 50 , width: 100, height: 100 )
        
       //videoFram.addSubview(gifViewToviedo)
      //  gifViewToviedo.frame = CGRect(x: videoFram.frame.width / 2  - 50 , y: videoFram.frame.height/2  - 50 , width: 100, height: 100 )
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        gifViewToviedo.isHidden = true

    }
    
    
    override func cancel() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.subview.alpha = 0
            self.workingSpaceView.alpha = 0
            self.barView.alpha = 0 
        }) { (true) in
            self.dismiss(animated: true, completion: nil)
        }
        

    }
    
}
