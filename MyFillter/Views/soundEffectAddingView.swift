//
//  soundEffectView.swift
//  MyFillter
//
//  Created by Ahmad on 7/7/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import AVFoundation
class soundEffectAddingView: UIView {
    
    var soundController : AddEffectViewController?
    var workinSpaceInstance : workingSpaceVc?
    var selectedSoundItem  : SoundEffectsObject?
    var tumbnails = Thumbnails()
    var player : AVPlayer?
    var isPlaying  = false
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        sliderValue.text = String(Int(soundSlider.value)) + " s"
        
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    lazy var  soundSlider : UISlider  = {
        let slider = UISlider()
        slider.setThumbImage(#imageLiteral(resourceName: "thumb"), for: .normal)
        slider.thumbTintColor = .red
        
      //  slider.thumbTintColor = UIColor.rgb(red: 237,green: 123,blue: 81)
        slider.addTarget(self, action: #selector(sliderAction), for: .valueChanged)
        slider.minimumTrackTintColor = .white
        slider.maximumTrackTintColor = .white
        
        slider.minimumValue = 0
        slider.maximumValue = 9
        return slider
    }()
    
    
    lazy var playPuseBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.imageView?.contentMode = .scaleAspectFill
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: 0)
        btn.setImage(#imageLiteral(resourceName: "play_circle"), for: .normal)
        btn.tintColor = UIColor.rgb(red: 237,green: 123,blue: 81)
        btn.addTarget(self, action: #selector(palyPauseAction), for: .touchUpInside)
        btn.backgroundColor = UIColor(white: 0, alpha: 0.6)
        btn.layer.cornerRadius = 10
        return btn
    }()
    lazy var  doneBtn:UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "done"), for: .normal)
        btn.tintColor = .red
        btn.addTarget(self, action: #selector(doneAction), for: .touchUpInside)
        return btn
    }()
    
    let titleImage : UIImageView = {
        let image = UIImageView(image: #imageLiteral(resourceName: "sound"))
        image.contentMode = .scaleAspectFit
        return image
    }()
    lazy var  removeBtn:UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "delete"), for: .normal)
        btn.tintColor = .gray
        btn.addTarget(self, action: #selector(removeAction), for: .touchUpInside)
        return btn
    }()
    
    
    let sliderValue : UILabel = {
        
        let lb = UILabel()
        lb.textColor = .white
        
        return lb
    }()
    
  
     func setupView(){
        
        self.backgroundColor = UIColor.rgb(red: 40, green: 40, blue: 40)
        self.addSubview(titleImage)
        self.addSubview(playPuseBtn)
        
        addSubview(soundSlider)
        
        
        
        _ = playPuseBtn.anchor(self.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 100)
        playPuseBtn.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        _ = titleImage.anchor(playPuseBtn.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 50, heightConstant: 50)
     
        self.addSubview(sliderValue)
        _ = sliderValue.anchor(playPuseBtn.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: -5 , leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        
        
        _ = soundSlider.anchor(titleImage.topAnchor, left: titleImage.rightAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 50)
        
        
       
        
        sliderValue.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        
        
        let stackBtn = UIStackView(arrangedSubviews: [doneBtn ,removeBtn])
        stackBtn.axis = .horizontal
        stackBtn.distribution = .fillEqually
        self.addSubview(stackBtn)
        _ = stackBtn.anchor(nil, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 50, bottomConstant: 50, rightConstant: 50, widthConstant: 200, heightConstant: 0)
      
        
        
       
        
        let deadTime = DispatchTime.now() + .milliseconds(800)
        DispatchQueue.main.asyncAfter(deadline: deadTime) {
            
            self.palyPauseAction()
        }
        
    }
    
    @objc func  doneAction(){
    
        
        
        let barView = workinSpaceInstance?.soundsCell?.cell?.mainViewContaner
        
       
        tumbnails.workinSpackRef = workinSpaceInstance
        
        tumbnails.name.text = selectedSoundItem?.name
        
        tumbnails.soundItem = selectedSoundItem
        
        barView?.addSubview(tumbnails)
        
        var  scounds = Int ( CMTimeGetSeconds((player?.currentItem?.duration)!))
        
        
        let sliderValue = Int(soundSlider.value)
        let scallValue = workinSpaceInstance?.soundsCell?.scaleVule
        barView?.clipsToBounds = true
        if scounds <= 0 {
            scounds = 1
        }
        
        let fullWidth = barView?.frame.width
        
        
        let  thumbnailsWidht : Int = Int(scallValue!) * scounds
      
        let thumbnailsX = Int(fullWidth!) * sliderValue / 10
        if CGFloat( thumbnailsX) < fullWidth! {
            tumbnails.frame = CGRect(x: CGFloat(thumbnailsX), y: (barView?.frame.height)!/2 - 10 , width: CGFloat(thumbnailsWidht), height: 20)
        }else {
            
            tumbnails.frame = CGRect(x: thumbnailsX - thumbnailsWidht/2 , y: Int((barView?.frame.height)!/2 - 10), width: thumbnailsWidht, height: 20)
            
        }
     
        
        let secound = sliderValue
        tumbnails.timePositon = CMTime(seconds: Double(secound), preferredTimescale: 1)
        print(secound)
        tumbnails.soundItem?.startPoint = Double(secound)
        tumbnails.soundItem?.startingTime = Float( secound)
          self.workinSpaceInstance?.soundEffectsArray.append(tumbnails)
        player?.pause()
        soundController?.dismiss(animated: true, completion: nil)
        
        
        
        
        
        
        
        
        
        
        
    }
    
   
    
    @objc func removeAction(){
        player?.pause()
        
        soundController?.cancel()
        
    }
    
    
    @objc func  sliderAction(){
        let value = Int(soundSlider.value)
        
        sliderValue.text = String(value) + "  " + "s"
    }
    
    
    func setupPlayer(URL: URL){
        print(URL.path)
        player = AVPlayer(url: URL)
        let palyerLayer = AVPlayerLayer(player: player)
         self.playPuseBtn.layer.addSublayer(palyerLayer)
        palyerLayer.frame = self.playPuseBtn.frame
        player?.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
         isPlaying = false
        
        playPuseBtn.setImage(#imageLiteral(resourceName: "play_circle"), for: .normal)
        player?.seek(to: kCMTimeZero)
    }
    
    
    
    var  isfirstPlaying = true
  
 
    
    
    @objc func palyPauseAction(){
        if isfirstPlaying {
            setupPlayer(URL: (self.selectedSoundItem?.url)!)
            playPuseBtn.setImage(#imageLiteral(resourceName: "paus_circule"), for: .normal)
            isPlaying = true
            isfirstPlaying = false
        }else {
            
            if isPlaying {
                playPuseBtn.setImage(#imageLiteral(resourceName: "play_circle"), for: .normal)
                player?.pause()
                isPlaying = false
            }
            else {
                playPuseBtn.setImage(#imageLiteral(resourceName: "paus_circule"), for: .normal)
                player?.play()
                isPlaying = true
            }
        }
        
       
        
        
    }
    
}




class AddTextEffect : soundEffectAddingView {
    
    
    var textEffect : TextEffect?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupView() {
        
        
        
        self.backgroundColor = UIColor.rgb(red: 40, green: 40, blue: 40)
        
        self.addSubview(sliderValue)
        
        self.addSubview(titleImage)
        
        addSubview(soundSlider)
        
        
        _ = sliderValue.anchor(self.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        
        sliderValue.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        _ = titleImage.anchor(nil, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 50, heightConstant: 50)
        
        titleImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        _ = soundSlider.anchor(titleImage.topAnchor, left: titleImage.rightAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 50)
        
        let stackBtn = UIStackView(arrangedSubviews: [doneBtn ,removeBtn])
        stackBtn.axis = .horizontal
        stackBtn.distribution = .fillEqually
        self.addSubview(stackBtn)
        _ = stackBtn.anchor(soundSlider.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 50, bottomConstant: 50, rightConstant: 50, widthConstant: 200, heightConstant: 50)
        
        titleImage.image = UIImage(named: "effectIcone")
        
    }
    
    
    
    override func doneAction() {
        
    }
    
    
    override func removeAction() {
        cancelView()
    }
    
    
    
    func cancelView(){
        
        
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.frame = CGRect(x: 0, y: (self.workinSpaceInstance?.view.frame.height)!, width: (self.workinSpaceInstance?.view.frame.width)!, height: 0)
            
            self.titleImage.alpha =  0
            self.soundSlider.alpha = 0
            self.sliderValue.alpha = 0 
        
            
            
        }) { (true ) in
            
        }
        
        
        
    }
    
}
