//
//  Thumbnails.swift
//  MyFillter
//
//  Created by Ahmad on 7/7/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import AVFoundation
class Thumbnails: UIView {
    
    var lastlocation = CGPoint(x: 0, y: 0)
    var workinSpackRef : workingSpaceVc?
  
    var soundItem : SoundEffectsObject?
    var timePositon: CMTime?
    var parentViewWidht : CGFloat = 0
    
    var soundPlayer : AVPlayer?
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let doubleRecognizer = UITapGestureRecognizer(target: self, action: #selector(doubleTapAction))
        doubleRecognizer.numberOfTapsRequired = 1
        self.gestureRecognizers = [  doubleRecognizer  ]
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let image : UIView   = {
        let im = UIView()
        im.backgroundColor = UIColor.rgb(red: 237, green: 123, blue: 81)
        return im
    }()
    
    
    let name : UILabel = {
        let lb = UILabel()
        lb.text = "sound1"
        lb.font =  UIFont.systemFont(ofSize: 13)
        
        lb.textColor = .white
        lb.textAlignment = .center
        return lb
    }()
    
    
    @objc func doubleTapAction(){
        name.isHidden = true

        let controller = SoundEffectsController()
        controller.workInctance = workinSpackRef
        controller.soundView.tumbnails = self
        controller.soundView.workinSpaceInstance = workinSpackRef
        controller.modalPresentationStyle = .overCurrentContext
        controller.soundView.playPuseBtn.setTitle(soundItem?.name, for: .normal)
        controller.soundView.selectedSoundItem = soundItem
        controller.soundView.soundSlider.value =  Float(self.frame.origin.x/parentViewWidht)
        self.workinSpackRef?.present(controller, animated: true, completion: nil)
    }
    

  
   
    
 
    private  func setupView(){
        self.name.isHidden = true
        self.addSubview(image)
        self.addSubview(name)
        _  = image.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = name.anchor(image.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    
    
    func setupSoundPlayer(){  
        guard let url = self.soundItem?.url else {
            return
        }
        print(url)
        self.soundPlayer = AVPlayer(url: url)
        let  soundPlayerLayer = AVPlayerLayer(player: soundPlayer)
        self.layer.addSublayer(soundPlayerLayer)
        soundPlayerLayer.frame = self.frame
        
}
    
    
    
}








