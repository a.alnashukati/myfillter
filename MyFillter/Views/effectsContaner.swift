//
//  effectsContaner.swift
//  MyFillter
//
//  Created by Ahmad on 7/16/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
class effectsContaner : UICollectionViewCell {
    var cellColor =  UIColor.rgb(red: 211,green: 209,blue: 212)
    override init(frame: CGRect) {
        super.init(frame: frame )
       setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    let startIndicator : UIImageView = {
        let image = UIImageView(image: #imageLiteral(resourceName: "back"))
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    let endIndicator : UIImageView = {
        let image = UIImageView(image: #imageLiteral(resourceName: "forward"))
        image.contentMode = .scaleAspectFit

        return image
    }()
    
    var  mainViewContaner: UIView = {
        var view = UIView()
        view.backgroundColor = .clear
        return view
        
    }()
    
    
    private func setupView(){
        self.layer.borderColor = cellColor.cgColor
        self.layer.borderWidth = 3
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.addSubview(startIndicator)
        self.addSubview(mainViewContaner)
        self.addSubview(endIndicator)

        startIndicator.backgroundColor = cellColor
        endIndicator.backgroundColor = cellColor
        _ = startIndicator.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 0)
        _ = endIndicator.anchor(self.topAnchor, left: nil, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 0)
        _ = mainViewContaner.anchor(self.topAnchor, left: startIndicator.rightAnchor, bottom: self.bottomAnchor, right: endIndicator.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

    }
    
}
