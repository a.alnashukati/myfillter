//
//  EfeectsThumbnails.swift
//  MyFillter
//
//  Created by Ahmad on 9/19/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
class EfeectsThumbnails : UIView  {
    
    var workinSpackRef : workingSpaceVc?
    var gifView : GifEffectView?
    var barView : UIView?
    var efferct : EffectsObject?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let doubleRecognizer = UITapGestureRecognizer(target: self, action: #selector(doubleTapAction))
        doubleRecognizer.numberOfTapsRequired = 1
        self.gestureRecognizers = [  doubleRecognizer  ]
        self.backgroundColor = .rgb(red: 120, green: 201, blue: 220)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc func doubleTapAction(){
       
        
        self.workinSpackRef?.editEffectForUrl(effectThumb: self )
    
    }
        
        
   
}
