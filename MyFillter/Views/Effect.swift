//
//  Effect.swift
//  MyFillter
//
//  Created by Ahmad on 11/25/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
class Effect : UIView {
    
    
    private var parentFrame : CGRect?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.autoresizesSubviews = false
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(movingFunc(recognizer:)))
        let scalingrecognizer  = UIPinchGestureRecognizer(target: self, action: #selector(scalingFunction(sender:)))
        self.gestureRecognizers = [scalingrecognizer]
        self.gestureRecognizers?.append(panRecognizer)
        
        let rotationRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(rotate(_:)))
        self.gestureRecognizers?.append(rotationRecognizer)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
 
    
    var lastLocation = CGPoint(x: 0, y: 0)
    
    @objc func  movingFunc (recognizer : UIPanGestureRecognizer){
        
        
        
        let transaction = recognizer.translation(in: self.superview)
        
        
        self.center = CGPoint(x: lastLocation.x + transaction.x, y: lastLocation.y + transaction.y )
        
        print(CGPoint(x: lastLocation.x + transaction.x, y: lastLocation.y + transaction.y ))
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.superview?.bringSubview(toFront: self)
        lastLocation = self.center
    }
    
    
    @objc func rotateAction(sender : UIRotationGestureRecognizer){
        let dgree = sender.rotation
        let radians = dgree / 180.0 * CGFloat.pi
        let rotation = self.transform.rotated(by: radians)
        self.transform = rotation
    }
    
    @objc func scalingFunction(sender : UIPinchGestureRecognizer){
    self.transform =  CGAffineTransform(scaleX: sender.scale, y: sender.scale)
        }
    
   
  
    
    
    func setParentFram(frame : CGRect){
        self.parentFrame = frame
    }
    
    
    @objc func rotate(_ gesture: UIRotationGestureRecognizer) {
      //  self.transform = self.transform.rotated(by: gesture.rotation)
    }
}



