//
//  GifEffectView.swift
//  MyFillter
//
//  Created by Ahmad on 9/17/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import FLAnimatedImage
class GifEffectView : Effect {
  
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        setupView()
        gifView.isUserInteractionEnabled = true
      
    
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    
   
  private  let gifView : FLAnimatedImageView = {
        let view = FLAnimatedImageView()
        view.contentMode = .scaleAspectFill

        return view
    }()
    
    
    func setGifForView(Url : URL ){
        
       let que = DispatchQueue(label: "gif")
        que.async {
            do {
                let data = try Data(contentsOf: Url)
                let gifimage = FLAnimatedImage(animatedGIFData: data)
                
                DispatchQueue.main.async {
                self.gifView.animatedImage = gifimage
                }
                
            }catch{
                
            }
        }
        
        
        
        
    }
    
    

    private func setupView(){
         
        
        self.addSubview(gifView)
        
        _ = gifView.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 5, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        
      
        
    }
    
    
    
    
   
}








