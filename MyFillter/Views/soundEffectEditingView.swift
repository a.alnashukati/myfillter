//
//  soundEffectEditingView.swift
//  MyFillter
//
//  Created by Ahmad on 7/7/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
class soundEffectEditingView : soundEffectAddingView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(300)) {
            self.soundSlider.value  = (self.tumbnails.soundItem?.startingTime)!
            self.sliderValue.text = String(Int((self.tumbnails.soundItem?.startingTime)!)) + "s"
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   @objc override func removeAction() {
   


     let thumbIndex = self.workinSpaceInstance?.soundEffectsArray.lastIndex(of: self.tumbnails)
    self.workinSpaceInstance?.soundEffectsArray.remove(at: thumbIndex!)
    self.player?.pause()
    self.tumbnails.removeFromSuperview()

    self.soundController?.dismiss(animated: true, completion: nil)
    }
    
    
    override func doneAction() {
        
        let barView = workinSpaceInstance?.soundsCell?.cell?.mainViewContaner
        
        
        tumbnails.workinSpackRef = workinSpaceInstance
        
        tumbnails.name.text = selectedSoundItem?.name
        
        tumbnails.soundItem = selectedSoundItem
        
        barView?.addSubview(tumbnails)
        
        var  scounds = Int ( CMTimeGetSeconds((player?.currentItem?.duration)!))
        
        
        let sliderValue = Int(soundSlider.value)
        let scallValue = workinSpaceInstance?.soundsCell?.scaleVule
        barView?.clipsToBounds = true
        if scounds <= 0 {
            scounds = 1
        }
        
        let fullWidth = barView?.frame.width
        
        
        let  thumbnailsWidht : Int = Int(scallValue!) * scounds
        
        let thumbnailsX = Int(fullWidth!) * sliderValue / 10
        if CGFloat( thumbnailsX) < fullWidth! {
            tumbnails.frame = CGRect(x: CGFloat(thumbnailsX), y: (barView?.frame.height)!/2 - 10 , width: CGFloat(thumbnailsWidht), height: 20)
        }else {
            
            tumbnails.frame = CGRect(x: thumbnailsX - thumbnailsWidht/2 , y: Int((barView?.frame.height)!/2 - 10), width: thumbnailsWidht, height: 20)
            
        }
        
        
        let secound = sliderValue
        tumbnails.timePositon = CMTime(seconds: Double(secound), preferredTimescale: 1)
        print(secound)
        tumbnails.soundItem?.startPoint = Double(secound)
        tumbnails.soundItem?.startingTime = Float( secound)
        player?.pause()
        soundController?.dismiss(animated: true, completion: nil)
        
        
        
        
        
        
        
        
    }
    
    
    
   
    
}
