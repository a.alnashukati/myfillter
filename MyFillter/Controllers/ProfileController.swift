//
//  ProfileController.swift
//  MyFillter
//
//  Created by Ahmad on 12/14/19.
//  Copyright © 2019 Ahmad. All rights reserved.
//

import Foundation
class ProfileController : baseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupView()
    }
    
    func setupNavigation(){
        let title = UILabel()
              title.text = "الملف الشخصي"
              title.font = UIFont.getBen(size: 18)
              title.textColor = .white
              navigationItem.titleView = title
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.isHidden = false

                    self.navigationController?.navigationBar.barTintColor = .backgroundColor
        
    }
    
    lazy var logInButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("تسجيل الدخول", for: .normal)
        btn.titleLabel?.font = UIFont.getBen(size: 15)
        btn.tintColor = .white
        btn.backgroundColor = .black
        btn.layer.cornerRadius = 10
        btn.addTarget(self, action: #selector(logInAction), for: .touchUpInside)
        return btn
    }()
    
    func setupView(){
        
        if helper.islogIn() {
            
        }else {
            
            let loginImage = UIImageView(image: #imageLiteral(resourceName: "smile - sad-1").withRenderingMode(.alwaysTemplate))
            loginImage.tintColor = .white
            view.addSubview(loginImage)
            loginImage.translatesAutoresizingMaskIntoConstraints = false
            loginImage.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
            loginImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            loginImage.heightAnchor.constraint(equalToConstant: 150 ).isActive = true
            loginImage.widthAnchor.constraint(equalToConstant: 150).isActive = true
            
            view.addSubview(logInButton)
            _ = logInButton.anchor(loginImage.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 200, heightConstant: 50)
            logInButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        }
        
        
    }
    
    @objc func logInAction(){
        let loginvc = loginVc()
        self.present(loginvc, animated: true, completion: nil)
    }
    

}
