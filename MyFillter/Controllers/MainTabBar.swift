//
//  MainTabBar.swift
//  MyFillter
//
//  Created by Ahmad on 11/17/19.
//  Copyright © 2019 Ahmad. All rights reserved.
//

import UIKit
class MainTabBar : UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .backgroundColor
       self.tabBarController?.tabBar.isTranslucent = false
        tabBar.barTintColor = UIColor.backgroundColor
               tabBar.isTranslucent = false
               self.tabBar.unselectedItemTintColor = UIColor.gray
        self.tabBar.tintColor = .white
      UITabBar.appearance().layer.borderWidth = 0.0
    UITabBar.appearance().clipsToBounds = true
        setupTabs()
        selectedIndex = 1
    }
    
    
    
  private  func setupTabs(){
        let feedsController = UINavigationController(rootViewController: FeedsController())
    feedsController.tabBarItem.image = #imageLiteral(resourceName: "home_tap")
    let creatVideo = UINavigationController(rootViewController:homeVc())
      creatVideo.tabBarItem.image = #imageLiteral(resourceName: "make_video")
    let MyVideos = UINavigationController(rootViewController:ProfileController())
         MyVideos.tabBarItem.image = #imageLiteral(resourceName: "my_Videos")
    viewControllers = [creatVideo  ,feedsController ,MyVideos
    ]

    }
    
    
}
