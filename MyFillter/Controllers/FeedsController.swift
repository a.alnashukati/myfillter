//
//  HomeController.swift
//  MyFillter
//
//  Created by Ahmad on 11/17/19.
//  Copyright © 2019 Ahmad. All rights reserved.
//

import UIKit
import Hero
import AVKit
class FeedsController: baseViewController {
 
    private let  feedsCellId = "cellIDCell"
    private let threadGroup = DispatchGroup()
    var feeds = [Feeds]()
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        getData()
        navigationController?.navigationBar.isHidden = false
        

        threadGroup.notify(queue: .main) {
            self.setupView()
        }
        
       setupNavigationBar()
        
    }
    
    
    
    func setupNavigationBar(){
        let title = UILabel()
        title.text = "أحدث الفيديوهات"
        title.font = UIFont.getBen(size: 18)
        title.textColor = .white
        navigationItem.titleView = title
        self.navigationController?.navigationBar.isTranslucent = false
              self.navigationController?.navigationBar.barTintColor = .backgroundColor
    }
    
    func setupView(){
        view.addSubview(videosCollection)
        
        _ = videosCollection.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
    }
    
    
    func getData(){
        threadGroup.enter()
        
        AuthService.inctance.getFeeds { (feeds) in
            self.feeds = feeds
            self.threadGroup.leave()

        }
       
            
            
        }
    
    
    
    lazy var videosCollection : UICollectionView = {
        let layout = FeedsLayout()
        layout.delegate = self
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource = self
        collection.register(FeedsCell.self, forCellWithReuseIdentifier: feedsCellId)
        collection.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        collection.backgroundColor = .clear
        collection.showsVerticalScrollIndicator = false
        return collection
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        videosCollection.reloadData()
     
    }
    
    func generateThumbnail(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(3, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }

}







extension FeedsController :UICollectionViewDelegate , UICollectionViewDelegateFlowLayout , UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return feeds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: feedsCellId, for: indexPath) as! FeedsCell
        let item = feeds[indexPath.item]
        cell.feedsImage.image = item.thumbImage
        cell.hero.id  = HeroId.video_id.rawValue
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 10)) / 2

                return CGSize(width: itemSize , height: itemSize)

    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      let item = feeds[indexPath.item]

        
        let palyer  = AVPlayer(url: URL(string: item.getUrl())!)
        let palyercontroller = AVPlayerViewController()
        palyercontroller.player = palyer
        self.present(palyercontroller, animated: true) {
            palyercontroller.player?.play()
        }
        
        
       let player = PlayVideoController()
        player.url = item.getUrl()
      self.navigationController?.pushViewController(player, animated: true )
        
    }
   
    
    
    
}


extension FeedsController :FeedsLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        let item = feeds[indexPath.item]
     
        return ((item.thumbImage?.size.height)!) * 0.3 + 30 //((feeds[indexPath.item].thumbImage?.size.height)! * 0.3 )
            
            
    }
    
    
}



protocol FeedsLayoutDelegate {
    func collectionView(
    _ collectionView: UICollectionView,
    heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
}

class FeedsLayout : UICollectionViewLayout {
    
    // 1
       var delegate: FeedsLayoutDelegate?

     // 2
     private let numberOfColumns = 2
     private let cellPadding: CGFloat = 6

     // 3
     private var cache: [UICollectionViewLayoutAttributes] = []

     // 4
     private var contentHeight: CGFloat = 0

     private var contentWidth: CGFloat {
       guard let collectionView = collectionView else {
         return 0
       }
       let insets = collectionView.contentInset
       return collectionView.bounds.width - (insets.left + insets.right)
     }

     // 5
     override var collectionViewContentSize: CGSize {
       return CGSize(width: contentWidth, height: contentHeight)
     }
     
     override func prepare() {
       // 1
       guard
         cache.isEmpty == true,
         let collectionView = collectionView
         else {
           return
       }
       // 2
       let columnWidth = contentWidth / CGFloat(numberOfColumns)
       var xOffset: [CGFloat] = []
       for column in 0..<numberOfColumns {
         xOffset.append(CGFloat(column) * columnWidth)
       }
       var column = 0
       var yOffset: [CGFloat] = .init(repeating: 0, count: numberOfColumns)
         
       // 3
       for item in 0..<collectionView.numberOfItems(inSection: 0) {
         let indexPath = IndexPath(item: item, section: 0)
           
         // 4
         let photoHeight = delegate?.collectionView(
           collectionView,
           heightForPhotoAtIndexPath: indexPath) ?? 180
         let height = cellPadding * 2 + photoHeight
         let frame = CGRect(x: xOffset[column],
                            y: yOffset[column],
                            width: columnWidth,
                            height: height)
         let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
           
         // 5
         let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
         attributes.frame = insetFrame
         cache.append(attributes)
           
         // 6
         contentHeight = max(contentHeight, frame.maxY)
         yOffset[column] = yOffset[column] + height
           
         column = column < (numberOfColumns - 1) ? (column + 1) : 0
       }
     }
     
     override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
       var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
       
       // Loop through the cache and look for items in the rect
       for attributes in cache {
         if attributes.frame.intersects(rect) {
           visibleLayoutAttributes.append(attributes)
         }
       }
       return visibleLayoutAttributes
     }
     
     override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
       return cache[indexPath.item]
     }
}

