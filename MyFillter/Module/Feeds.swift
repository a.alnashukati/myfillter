//
//  Feeds.swift
//  MyFillter
//
//  Created by Ahmad on 12/14/19.
//  Copyright © 2019 Ahmad. All rights reserved.
//

import UIKit
class Feeds {
    
    private var id : String
    private var videoUrl : String
    private var thumb:String
    var thumbImage : UIImage?
    init( id : String,videoUrl : String , thumb : String) {
        self.id = id 
        self.videoUrl = videoUrl
        self.thumb  = thumb
      
    }
    
    func getThumbStr()->String{
        return thumb
    }
    
    func getUrl()-> String{
        return videoUrl
    }
    
    func setImage(image : UIImage?){
        self.thumbImage = image
    }
}
