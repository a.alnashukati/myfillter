//
//  EffectsLayer .swift
//  MyFillter
//
//  Created by Ahmad on 7/21/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import Foundation
class EffectsObject : NSObject {
    
    
    var LayerFramInWorkspace : CGRect?
    var BeginTime : CFTimeInterval?
    var Layer = CALayer()
    var url : URL?
    var statPoint : Int?
    override init() {
        super.init()
    }
    
    init( With FramInWorkspace :CGRect  ,URL : URL, and BeginingTime : CFTimeInterval){
        super.init()
        self.LayerFramInWorkspace = FramInWorkspace
        self.BeginTime = BeginingTime
        self.url = URL
       // Layer.frame = Frame
        Layer.beginTime = BeginingTime
        
    }
    
    func setAntimationToLayer(Layer: CALayer , with url : URL){
        
        
        DispatchQueue.global(qos: .background).async {
            let helper = GifHelper()
            let animation = helper.animationForGif(with: url)
            Layer.add(animation!, forKey: "gif")
        }
       
    }
    
    
    
}
