//
//  effectObject.swift
//  MyFillter
//
//  Created by Ahmad on 7/21/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import Foundation
class SoundEffectsObject :NSObject {
    
    var url:URL?
    var startingTime : Float?
    var Duration : Float?
    var startPoint : Double?
    var name : String? 
    override init() {
        super.init()
    }
    
    init(url: URL , at Time : Float , duration: Float){
        self.url = url
       self.startingTime = Time
        self.Duration = duration
    }
    
    
    
    init(url: URL , at Time : Float ){
        self.url = url
        self.startingTime = Time
    }
    
    
    
    
}
