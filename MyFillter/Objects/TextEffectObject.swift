//
//  TextEffectObject.swift
//  MyFillter
//
//  Created by Ahmad on 12/5/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import Foundation
class TextEffectObject : NSObject {
    
    
    var animationKeys : [CAKeyframeAnimation]?
    var startAt : Float?
    var frame : CGRect?
    var textLayer : CATextLayer?
    
    
    func setAnimationToLayers(){
        
        for anim in animationKeys! {
            
            self.textLayer?.add(anim, forKey: nil)
        }
        
        
    }
    
    
    
    
    
}
