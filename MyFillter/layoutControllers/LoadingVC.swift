//
//  LoadingVC.swift
//  MyFillter
//
//  Created by Ahmad on 9/30/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import Foundation
class LoadingVC: MoveMakerVc {
    
    var uiImages = [UIImage]()
    let workingSpace = workingSpaceVc()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       lable.text = "يرجى الانتظار "
        
    }
    
    
    override func threadNotification() {
        threadGroup.notify(queue: .main) {
            
            let deadTime = DispatchTime.now() + .milliseconds(1000)
            DispatchQueue.main.asyncAfter(deadline: deadTime, execute: {
                self.navigationController?.pushViewController(self.workingSpace, animated: true )

            })
        }
    }
    
    override func makeVideo(){
        
        
        threadGroup.enter()
        
        let settings = CXEImagesToVideo.videoSettings(codec: AVVideoCodecType.h264.rawValue, width: (uiImages[0].cgImage?.width)!, height: (uiImages[0].cgImage?.height)!)
        
        
        let movieMaker = CXEImagesToVideo(videoSettings: settings)
        
        movieMaker.createMovieFrom(images: uiImages ){ (fileURL:URL) in
            self.workingSpace.url = fileURL
            self.threadGroup.leave()
        }
        
    }
    
    
    
    
    
    
    
}
