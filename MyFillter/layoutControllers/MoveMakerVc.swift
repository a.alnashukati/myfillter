//
//  MoveMakerVc.swift
//  MyFillter
//
//  Created by Ahmad on 9/19/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import FLAnimatedImage

class  MoveMakerVc : baseViewController  {
    
   
    var  saveVideo = ExportVc()

    var threadGroup  = DispatchGroup()
    private  var soundEffectsArray = [SoundEffectsObject]()
    private  var effectArray = [EffectsObject]()
    private   var videoUrl : URL?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.makeVideo()
        
threadNotification()
        
        let path = Bundle.main.url(forResource: "loading", withExtension: "gif")!
        do {
            let data = try Data(contentsOf: path)
            let gifimage = FLAnimatedImage(animatedGIFData: data)
            
            imageView.animatedImage = gifimage
            
        }catch{
            
        }
    }
    
    
    func threadNotification(){
        
        
        threadGroup.notify(queue: .main) {
            self.navigationController?.pushViewController(self.saveVideo, animated: true )
            
            
            
        }
    }
    
    
    
    let logoImage : UIImageView = {
        let image = UIImageView(image: #imageLiteral(resourceName: "logo waite"))
        image.contentMode = .scaleAspectFill
       return image
    }()

    let imageView : FLAnimatedImageView = {
        let im = FLAnimatedImageView()
        im.contentMode = .scaleAspectFill
        return im 
    }()
    
    
    let lable : UILabel  = {
        let lb = UILabel()
        lb.text = "جاري تصدير الفيديو "
        lb.font = UIFont.getSemiBold(size: 20)
        lb.textColor = UIColor.white 
        lb.textAlignment = .center
        return lb
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
   
    
  private  let subView = UIView()

    fileprivate func setupView(){
        subView.backgroundColor = .clear
        
    
        
        view.addSubview(logoImage)
       _ = logoImage.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 100)
        logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImage.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true 
        
        
        view.addSubview(imageView)
        
        _ = imageView.anchor(logoImage.centerYAnchor, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 300, heightConstant: 100)
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        view.addSubview(lable)
        
        _ = lable.anchor(imageView.centerYAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
    }
    
    
    func setVideoInfo( url : URL, sounds : [Thumbnails] , stikers : [EffectsObject]){
        
        
        self.effectArray = stikers
        self.videoUrl = url
        for i in sounds {
            let object = i.soundItem
            soundEffectsArray.append(object!)
        }
    }
    
    
     func makeVideo(){
        
        
    
        threadGroup.enter()
       
        
        
        if soundEffectsArray.count == 0 {
            
            
            
            Gif_helper.addWaterMark(videoUrl: videoUrl!, with: effectArray) { (eror, url) in
                
                
                if let url = url {
                    self.saveVideo.outputURL = url
                    self.threadGroup.leave()

                }
            }
            
        }else {
            
            
            
            
            AudioHelper.mergeVideoAndAudio(videoUrl: videoUrl!, with: soundEffectsArray) { (error, UrlWithAoudio) in
                
                
                
                if let Url = UrlWithAoudio {
                    
                    Gif_helper.addWaterMark(videoUrl: Url, with: self.effectArray, completion: { (error,mvoeURL  ) in
                        
                        print(mvoeURL)
                        self.saveVideo.outputURL = mvoeURL!
                        self.threadGroup.leave()
                        
                    })
                    
                    
                }
              
            }
                
                
                
            }
            
            
            
        }
      
        
    
        
        
    }
    
    
    
    
    

