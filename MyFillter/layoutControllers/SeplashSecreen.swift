//
//  SeplashSecreen.swift
//  MyFillter
//
//  Created by Ahmad on 6/5/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import FLAnimatedImage
class SeplashSecreen :baseViewController  {
    
    
    
    
    var mainNavigationController = MainNavigationController()
   
    
 
    let   logoImage : FLAnimatedImageView = {
        let image = FLAnimatedImageView()
        return image
    }()
    
    override func viewDidLoad() {
        let   path  = Bundle.main.url(forResource: "logo", withExtension: ".gif")

        super.viewDidLoad()
        do {
            let gifData = try  Data(contentsOf: path!)
             let image =  FLAnimatedImage(animatedGIFData: gifData)
            logoImage.animatedImage = image
        }catch{
            
        }
        
        setupView()

        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.splashTimeOut(sender:)), userInfo: nil, repeats: false)


    }
    
   
    
    @objc func splashTimeOut(sender : Timer){
        
        self.present(mainNavigationController, animated: true) {
        }
        }

    
   
    
    func setupView(){
        
        view.addSubview(logoImage)
        logoImage.translatesAutoresizingMaskIntoConstraints = false
        logoImage.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.3).isActive = true
        let width = view.frame.height * 0.3
        logoImage.widthAnchor.constraint(equalToConstant: width).isActive = true
        logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive  = true
        logoImage.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
    }
    
    
}
