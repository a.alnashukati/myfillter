//
//  ExportVc.swift
//  MyFillter
//
//  Created by Ahmad on 5/24/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
class ExportVc: homeVc {
    
    let ExportArry = [UIImage(named: "export"),UIImage(named: "save")]
    let ExportTextArry = ["تصدير","حفظ في الاستوديو"]
    var outputURL : URL?
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true )
        self.navigationController?.navigationBar.isHidden = true 

    }
    

    
    lazy var cancelButton : UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "close"), for: .normal)
        btn.tintColor = .white 
        btn.addTarget(self, action: #selector(close), for: .touchUpInside)
        return btn 
    }()
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionBtn.frame.width-20, height: collectionBtn.frame.height * 0.3)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.item {
        case 0:
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [outputURL], applicationActivities:nil)
            activityViewController.excludedActivityTypes = [.print, .copyToPasteboard, .assignToContact, .saveToCameraRoll, .airDrop]
            
            DispatchQueue.main.async {
                self.present(activityViewController, animated: true, completion: nil);
                if let popOver = activityViewController.popoverPresentationController {
                    popOver.sourceView = self.view
                }
            }
            
            
            
        case 1 :
            UISaveVideoAtPathToSavedPhotosAlbum((outputURL?.path)!, nil, #selector(savigSucsess), nil)
               savigSucsess()
        default:
            break
        }
    }
    
    @objc func savigSucsess(){
        
        let alert = UIAlertController(title: nil, message: "تم حفط بنجاح ", preferredStyle: .alert)
        let action  = UIAlertAction(title: "OK", style: .default) { (action) in
            
        }
        alert.addAction(action)
        self.present(alert, animated: true , completion: nil )
        
    }

    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: view.frame.size.height * 0.1, left: 0, bottom: 0, right: 0  )
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
   
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! exportVcCell
        cell.imageBtn.image = ExportArry[indexPath.item]
        cell.btnTitel.text = ExportTextArry[indexPath.item]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ExportArry.count
    }
    @objc func close(){
        self.navigationController?.popToRootViewController(animated: true )
        
    }
    
    
   
   
    
    override func setupView() {
       
        view.addSubview(cancelButton)
        view.addSubview(collectionBtn)
        collectionBtn.register(exportVcCell.self, forCellWithReuseIdentifier: cellId)

  _ = cancelButton.anchor(view.topAnchor, left: nil, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 50, heightConstant: 50)
        
        _ = collectionBtn.anchor(nil, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant:( view.frame.height * 2) / 3 )
        
        collectionBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectionBtn.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        
    }
    
    
    
    
    

}



class exportVcCell:btnCell  {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let btnTitel : UILabel  = {
        let lb = UILabel()
        lb.font = UIFont.getSemiBold(size: 20)
        lb.textColor = .white
        lb.textAlignment = .center 
        return lb
    }()
 
    
    
    override func setupView() {
        self.addSubview(imageBtn)
      
        _ = imageBtn.anchor(self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
      
        self.addSubview(btnTitel)
        
        _ = btnTitel.anchor(imageBtn.bottomAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant:-40 , leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        
        
    }
    
    
    
    
    
}

