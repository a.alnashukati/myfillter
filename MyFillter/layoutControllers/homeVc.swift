//
//  homeVc.swift
//  MyFillter
//
//  Created by Ahmad on 5/24/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit

class homeVc: baseViewController , UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var ImageArray:[UIImage?]?
    let homeArray = [UIImage(named: "wallpapers"),UIImage(named: "Camera"),UIImage(named: "galery")]

     let cellId = "CellId"
    lazy var  picker = UIImagePickerController()

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (ImageArray!.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! btnCell
        cell.imageBtn.image = ImageArray?[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionBtn.frame.width-20, height: collectionBtn.frame.height/3)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        ImageArray = homeArray
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.backgroundColor
        navigationItem.hidesBackButton = true
        setupView()
    
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true )
        navigationController?.navigationBar.isHidden = false

    }
    
    
    lazy var collectionBtn:UICollectionView  = {
        let layout = UICollectionViewFlowLayout()
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        collection.isScrollEnabled = false
        return collection
    }()
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.item {
        case 0:
            let cc = ItemsCollection()
            cc.navigationItem.backBarButtonItem?.title = ""
            self.navigationController?.pushViewController(cc, animated: true)
        case 1:
            print(123)
            let takeVeido = CaptureVideoController()
            self.navigationController?.pushViewController(takeVeido, animated: true)
        case 2:
            
             if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                
                picker.delegate = self
                picker.sourceType = .savedPhotosAlbum
               picker.allowsEditing = false
                self.present(picker, animated: true, completion: nil)
                
            }
            
        default:
            let cc = workingSpaceVc()
            self.present(cc, animated: true, completion: nil)

        }
    }
    
      func setupView(){
        collectionBtn.register(btnCell.self, forCellWithReuseIdentifier: cellId)
        view.addSubview(collectionBtn)
        _  =  collectionBtn.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 50, leftConstant: 100, bottomConstant: 100, rightConstant:100, widthConstant: 0, heightConstant: 0)
    }
  
    
  private  var threadGroup = DispatchGroup()

}




extension homeVc : UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    
    
    
     @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {


        let loading = LoadingVC()
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            let image1 = pickedImage
            let image2 = pickedImage
           loading.uiImages.append(image1)
           loading.uiImages.append(image2)
            
            
            DispatchQueue.global(qos: .background).async {
            
                DispatchQueue.main.async {
                    
          
                    picker.dismiss(animated: true , completion: {
                        self.navigationController?.pushViewController(loading, animated: true )
                    })
                }
                
            }
            }
            
            
            
            
        }
    
        

        
       
    
}
