//
//  ItemsCollection.swift
//  MyFillter
//
//  Created by Ahmad on 5/24/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import AVFoundation
class ItemsCollection :baseViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let resorce = Bundle.main.paths(forResourcesOfType: "png", inDirectory: "backgrounds")
     var ImageArray = [UIImage]()
    let itemCellId = "CellId"
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resorce.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: itemCellId, for: indexPath) as! ItemsCells
        let Image = UIImage(imageLiteralResourceName: self.resorce[indexPath.item])

        
        DispatchQueue.global(qos: .background).async {
            self.ImageArray.append(Image)
            DispatchQueue.main.async {
                cell.item.image = Image

            }

            
        }
    
        
        return cell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        
        let btn = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backFunction(sender:)))
        btn.tintColor = .white 
        navigationItem.leftBarButtonItem = btn
        registerationCell()
        setupView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true )
        navigationController?.navigationBar.isHidden = false

    }
    
    
    @objc func backFunction(sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    lazy var itemsCollections:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource  = self
        collection.backgroundColor = .clear
        return collection
    }()
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 50, left: 10, bottom: 20, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let loading = LoadingVC()
        
    let image = ImageArray[indexPath.item]

        
    loading.uiImages.append(image)
     loading.uiImages.append(image)
       
        
        self.navigationController?.pushViewController(loading, animated: true )
        
    }
    
         func setupView(){
       
      view.addSubview(itemsCollections)
        _ = itemsCollections.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    
    }
    
     func registerationCell(){
        itemsCollections.register(ItemsCells.self , forCellWithReuseIdentifier: self.itemCellId)

    }
    
    
    
}



