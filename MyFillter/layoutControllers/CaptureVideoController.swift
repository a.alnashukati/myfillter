//
//  CaptureVideoController.swift
//  MyFillter
//
//  Created by Ahmad on 5/31/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import Photos
class CaptureVideoController : baseViewController  {
    
    
    let camer_Controller = CameraController()
    
    let capturePreviewView :UIView = {
        let view = UIView()
        return view
    }()
    
    let captureBtn :UIButton = {
        let btn = UIButton(type: .system)
        btn.addTarget(self, action:#selector(CaptureHandler), for: .touchUpInside)
        btn.setImage(#imageLiteral(resourceName: "videocam"), for: .normal)
        btn.layer.backgroundColor = UIColor.white.cgColor
        btn.layer.cornerRadius = 50
        btn.tintColor = .red
        return btn
    }()
    
    
    
    override func viewDidLoad() {
        camer_Controller.controller = self 
        
            func configureCameraController() {
                camer_Controller.prepare {(error) in
                    if let error = error {
                        print(error)
                    }
                    
                    try? self.camer_Controller.displayPreview(on: self.capturePreviewView)
                }
            }
        let btn = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backFunction(sender:)))
        btn.tintColor = .white
        navigationItem.leftBarButtonItem = btn
        
            setupView()
            configureCameraController()
        
        }
    
    
    @objc func backFunction(sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
  
    var isCaptureing = false
    @objc func CaptureHandler(){
    
        if !isCaptureing{
            camer_Controller.startCapture()
            captureBtn.setImage(#imageLiteral(resourceName: "stop"), for: .normal)
            isCaptureing = true
            
        }else {
            captureBtn.setImage(#imageLiteral(resourceName: "videocam"), for: .normal)
            camer_Controller.stopRecording()
           isCaptureing = false
        }
        
    }
    func setupView (){
        
        view.addSubview(capturePreviewView)
        
        _ = capturePreviewView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        capturePreviewView.addSubview(captureBtn)
    _ = captureBtn.anchor(nil, left: nil, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 50, rightConstant: 0, widthConstant:100, heightConstant: 100)
        captureBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true 
        
        
    }
    
    
}
