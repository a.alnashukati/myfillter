//
//  StickersEffectsVc.swift
//  MyFillter
//
//  Created by Ahmad on 6/30/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import FLAnimatedImage
class StickersEffectsVc: SoundsEffectsVc {

    var pathName = ""
    var  Resorce = Bundle.main.paths(forResourcesOfType: "gif", inDirectory: "/Effect")
    var catcontrollr :EffectsCategories?
    override func viewDidLoad() {
        super.viewDidLoad()
        titelImage.image = UIImage(named: "stickers")
        lableTitile.image = UIImage(named: "stickerslable")
        self.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        
          Resorce = Bundle.main.paths(forResourcesOfType: "gif", inDirectory: "/Effect/\(pathName)")


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Resorce.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: itemCellId, for: indexPath) as! StickerCells
        let url = URL(fileURLWithPath: self.Resorce[indexPath.item])
         cell.setImageFromUrl(url: url )
     
        return cell
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    /*
        let controller = AddSitckerEffectController()
        let navigation = UINavigationController(rootViewController: controller)
        navigation.modalPresentationStyle  = .overCurrentContext
        controller.workInctance = workinSpaceRef
      controller.url = url
        
        let contanerView = StickersControllerView()
        controller.contaner = contanerView
        controller.subview.backgroundColor = .clear
        contanerView.stikerController = controller
    contanerView.workinSpaceInstance = workinSpaceRef
        self.dismiss(animated: true) {
            self.workinSpaceRef?.present(navigation, animated: true, completion: nil)
            print (self.workinSpaceRef?.navigationController?.navigationBar.frame.size.height)
        }
         
         let contanerView = StickersControllerView()
         self.workinSpaceRef?.view.addSubview(contanerView)
         contanerView.frame = (self.workinSpaceRef?.bottomContaner.frame)!
        */
        let url = URL(fileURLWithPath: self.Resorce[indexPath.item])

        
        self.dismiss(animated: true) {
            self.catcontrollr?.dismiss(animated: true, completion: nil)

            self.workinSpaceRef?.addEffectFormUrl(url: url)
        }
        
    }

    
    override func registerationCell() {
        itemsCollections.register(StickerCells.self , forCellWithReuseIdentifier: self.itemCellId)

    }
 
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: 200   , height: 200)

        }
        else{
            return CGSize(width: 100   , height: 100)

        }
    }

}





class StickerCells : UICollectionViewCell{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let item : FLAnimatedImageView = {
        let im = FLAnimatedImageView(image: #imageLiteral(resourceName: "plasecholder") .withRenderingMode(.alwaysTemplate))
        im.tintColor = .white 
        im.contentMode = .scaleAspectFit
        return im
    }()
    
    
    
    func setImageFromUrl(url: URL ){
        
        let queue = DispatchQueue(label: "getAnimation ")
        queue.async {
            do {
                
                let data = try Data(contentsOf: url)
                let image =  FLAnimatedImage(animatedGIFData: data )
                
                DispatchQueue.main.async {
                    self.item.animatedImage = image

                }
                
                
                
            }catch{
                
            }
        }
        
        }
        
        
        
    
    
    
    func setupView(){
        self.addSubview(item)
        self.layer.cornerRadius = 10
        self.backgroundColor = UIColor(white: 1, alpha: 0.6)
        _ = item.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    
    }
    
    
}

