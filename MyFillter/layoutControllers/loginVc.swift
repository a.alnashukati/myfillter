//
//  ViewController.swift
//  MyFillter
//
//  Created by Ahmad on 5/19/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit
import AVFoundation
import FirebaseAuth
import FLAnimatedImage
class loginVc: baseViewController  , UITextFieldDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        let path = Bundle.main.url(forResource: "logo-my-filter", withExtension: "gif")!
        do {
            let data = try Data(contentsOf: path)
            let gifimage = FLAnimatedImage(animatedGIFData: data)
        
            logoView.animatedImage = gifimage
            
        }catch{
            
        }
        
        
          setupView()
        keyboardNotify()
        
    
    }

    
    lazy var creatAccountBtn: UIButton = {
       let btn = UIButton(type: .system)
        btn.setTitle("Creat account", for: .normal)
        btn.addTarget(self, action: #selector(ShowNewVC), for: .touchUpInside)
        return btn
    }()
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == PasswordTextField {
            
            textField.resignFirstResponder()
        }else   {
            textField.resignFirstResponder()
            PasswordTextField.becomeFirstResponder()
        }
      
        return false
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    let backGrounView : UIImageView = {
        let im = UIImageView(image: #imageLiteral(resourceName: "background"))
        im.contentMode = .scaleAspectFill
        return im
    }()
    
  private  let logoView:FLAnimatedImageView = {
    let image = FLAnimatedImageView()
    image.contentMode = .scaleAspectFit
        return image
    }()
    
  private  let lightImagView : UIView = {
        let im = UIView()
        im.backgroundColor = UIColor(white: 1, alpha: 0.5)
       im.layer.cornerRadius = 20
        return im
    }()
    
 private   let loginIconeView : UIImageView = {
        let im =  UIImageView(image: #imageLiteral(resourceName: "loginIcone"))
        im.contentMode = .scaleAspectFill
    
        return im
    }()
    
  private  let usernamImageView: UIImageView = {
        let im = UIImageView(image: #imageLiteral(resourceName: "username"))
        im.contentMode = .scaleAspectFill
        return im
    }()
    
  private  let passwordImageView: UIImageView = {
        let im = UIImageView(image:#imageLiteral(resourceName: "password"))
        im.contentMode = .scaleAspectFill
        return im
    }()
    
  private  lazy var  logInButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Log in", for: .normal)
        btn.tintColor = .white
        btn.backgroundColor = UIColor.rgb(red: 21, green: 122, blue: 254)
        btn.addTarget(self, action: #selector(logInHandeler), for: .touchUpInside)
        btn.layer.cornerRadius = 5
        return btn
    }()
    
  private  lazy var  usernameTextField: UITextField = {
        let lb = UITextField()
        lb.textColor = .gray
        lb.textAlignment = .left
        lb.placeholder = "email"
         lb.delegate = self
    
        lb.keyboardType = .emailAddress

        return lb
    }()
    
  private   lazy var PasswordTextField: UITextField = {
        let lb = UITextField()
        lb.textColor = .gray
        lb.textAlignment = .left
        lb.placeholder = "Password"
        lb.isSecureTextEntry = true
    lb.delegate = self

        return lb
    }()
    
    let usernameSubView :UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        return view
    }()
    
    let PasswordSubView :UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5

        return view
    }()

     func setupView(){
        
        
          view.addSubview(backGrounView)
           _ = backGrounView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        view.addSubview(logoView)
        
        view.addSubview(lightImagView)
        view.addSubview(loginIconeView)

          lightImagView.addSubview(usernameSubView)
          lightImagView.addSubview(PasswordSubView)
        
        lightImagView.addSubview(logInButton)
        
        if UIDevice.current.userInterfaceIdiom == .pad  {
            _ = logoView.anchor(view.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 50, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 400, heightConstant: 300)
            logoView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            
             _  =  lightImagView.anchor(nil, left: nil, bottom: nil, right: nil, topConstant:0 , leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 400, heightConstant: 400)
        lightImagView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
       lightImagView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 50).isActive = true
           
            _ = loginIconeView.anchor(lightImagView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: -50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 100)
            loginIconeView.centerXAnchor.constraint(equalTo: lightImagView.centerXAnchor).isActive = true
           
            _ = usernameSubView.anchor(lightImagView.topAnchor, left: lightImagView.leftAnchor, bottom: nil, right: lightImagView.rightAnchor, topConstant: 100, leftConstant: 30, bottomConstant: 0, rightConstant: 30, widthConstant: 0, heightConstant: 60)
            
            _ = PasswordSubView.anchor(usernameSubView.bottomAnchor, left: usernameSubView.leftAnchor, bottom: nil, right: usernameSubView.rightAnchor, topConstant: 50, leftConstant: 0, bottomConstant: 0, rightConstant:0 , widthConstant: 0, heightConstant: 60)
            
          
          
            
        } else {
          
            _ = logoView.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 100, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 100, heightConstant: 150)
            
            _  =  lightImagView.anchor(logoView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: 0, heightConstant: 300)
            
            
            _ = loginIconeView.anchor(lightImagView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: -35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 75, heightConstant: 75)
              loginIconeView.centerXAnchor.constraint(equalTo: lightImagView.centerXAnchor).isActive = true
          
            _ = usernameSubView.anchor(lightImagView.topAnchor, left: lightImagView.leftAnchor, bottom: nil, right: lightImagView.rightAnchor, topConstant: 50, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 50)
            
          _ = PasswordSubView.anchor(usernameSubView.bottomAnchor, left: usernameSubView.leftAnchor, bottom: nil, right: usernameSubView.rightAnchor, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant:0 , widthConstant: 0, heightConstant: 50)
            
            
            
        }
        
     
        
        usernameSubView.addSubview(usernamImageView)
        _ = usernamImageView.anchor(usernameSubView.topAnchor, left: usernameSubView.leftAnchor, bottom: usernameSubView.bottomAnchor, right: nil, topConstant: 5, leftConstant: 5, bottomConstant: 5, rightConstant: 0, widthConstant: 50, heightConstant: 50)
      
        usernameSubView.addSubview(usernameTextField)
        _ = usernameTextField.anchor(usernameSubView.topAnchor, left: usernamImageView.rightAnchor, bottom: usernameSubView.bottomAnchor, right: usernameSubView.rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 5, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        
    PasswordSubView.addSubview(passwordImageView)
       _ = passwordImageView.anchor(PasswordSubView.topAnchor, left: PasswordSubView.leftAnchor, bottom: PasswordSubView.bottomAnchor, right: nil, topConstant: 5, leftConstant: 5, bottomConstant: 5, rightConstant: 0, widthConstant: 50, heightConstant: 50)
      
        PasswordSubView.addSubview(PasswordTextField)
      _ = PasswordTextField.anchor(PasswordSubView.topAnchor, left: passwordImageView.rightAnchor, bottom: PasswordSubView.bottomAnchor, right: PasswordSubView.rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 5, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        
        lightImagView.addSubview(logInButton)
        _ = logInButton.anchor(PasswordSubView.bottomAnchor, left: lightImagView.leftAnchor, bottom: nil, right: lightImagView.rightAnchor, topConstant: 30, leftConstant: 75, bottomConstant: 0, rightConstant: 75, widthConstant: 0, heightConstant: 50)
           logInButton.centerXAnchor.constraint(equalTo: lightImagView.centerXAnchor).isActive = true
        
        let yourAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.blue,
            .underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
        
        let attributeString = NSMutableAttributedString(string: "Creat account",
                                                        attributes: yourAttributes)
        
        self.creatAccountBtn.setAttributedTitle(attributeString, for: .normal)
        
        view.addSubview(creatAccountBtn)
        _ = creatAccountBtn.anchor(lightImagView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 30)
        creatAccountBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        

        }
    
    
    @objc func ShowNewVC(){
        let newAccount = CreatAccountVC()
        self.present(newAccount, animated: true, completion: nil)
    }
    
    @objc  func logInHandeler(){
        
      
        
        
   let home  = MainNavigationController()
            print(123)
            let email = usernameTextField.text
            let password = PasswordTextField.text
        Auth.auth().signIn(withEmail: email!, password: password!) { (user, error) in
            
            if user?.uid == nil {
                
                Auth.auth().createUser(withEmail: self.usernameTextField.text!, password: self.PasswordTextField.text!, completion: { (user, creationError) in
                    
                    if creationError != nil {
                        self.alertFunction(message: (creationError?.localizedDescription.description)!)
                        
                    }else {
                        print(123)
                        helper.setuesrId(uid: user?.uid)
                        print(helper.islogIn())
                        self.navigationController?.pushViewController(home, animated: true)
                    }
                        
                    })
                
                }
            else {
                if error != nil {
                    
                    self.alertFunction(message: (error?.localizedDescription.description)!)
                }else{
                    
                    helper.setuesrId(uid: user?.uid)
                    self.present(home, animated: true , completion: nil )

            }
            }
            
            }
            
        
}
    
    
    private func alertFunction(message : String){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    internal func keyboardNotify(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(keybordShowing), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keybordHide), name: .UIKeyboardWillHide, object: nil)
        
        
    }
    @objc func keybordShowing(){
        UIView.animate(withDuration: 0.5, animations: {
            self.view.frame = CGRect(x: 0, y: -100, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    }
    
    @objc func keybordHide(){
        UIView.animate(withDuration: 0.5, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)    }
    
}

