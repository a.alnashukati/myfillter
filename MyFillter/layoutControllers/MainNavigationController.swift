//
//  MainNavigationController.swift
//  MyFillter
//
//  Created by Ahmad on 5/24/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit

class MainNavigationController : UINavigationController {
    var controller : SeplashSecreen?
    
   
    
    override func viewDidLoad() {
       
        
        BaseControllerObject.shared.baseNavigationController = self 
        self.navigationBar.barTintColor = UIColor.rgb(red: 57, green: 56, blue: 56)
        navigationBar.isTranslucent = false
        super.viewDidLoad()
        let home = homeVc()
        let login = loginVc()
        controller = nil
        viewControllers = [home]

    if helper.islogIn() {
            viewControllers = [home]
        }else{
            viewControllers = [login]
        }
    }
    

}
