//
//  workingSpaceVc .swift
//  MyFillter
//
//  Created by Ahmad on 5/24/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
import AVFoundation
class workingSpaceVc :baseViewController , UICollectionViewDelegate ,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout    {
    
  //  var identity = CGAffineTransform.identity

    var iconsImages = [UIImage(named : "saounds") , UIImage(named: "stickers"), UIImage(named: "text")]
    private let btnCellString = "CellId"
   
    var url :URL?
    var isSetWidth = false

    


    
    var videoPlayer : VideoPlayer = {
        let vv = VideoPlayer()
        return vv
    }()

    var soundsCell  : EffectsBarsCell?
    var effectsCell  : EffectsBarsCell?
    var textCell  : EffectsBarsCell?
    
    var soundEffectsArray = [Thumbnails]()
    var effectArray = [EffectsObject]()
    var textArray = [TextEffectObject]()
    
    lazy var collectionBtns : UICollectionView  = {
        let layout = UICollectionViewFlowLayout()
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource  = self
        collection.backgroundColor = .clear
        return collection
    }()
    
    
  
    
    
    let timeInspiterline : UIView = {
        let view = UIView()
        view.backgroundColor = .red
        return view
    }()
    
    
    
    lazy var palyBtn : UIButton = {
        let btn = UIButton(type: .system )
        btn.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        btn.addTarget(self, action: #selector(playAction), for: .touchUpInside)
        return btn
    }()
    
    
    let startScounds : UILabel = {
        let lb = UILabel()
        lb.textAlignment = .center
        lb.textColor = .white
        lb.text = "00:00"
        return lb
    }()
    
    
    let remaingScounds : UILabel = {
        let lb = UILabel()
        lb.textAlignment = .center
        lb.textColor = .white

        lb.text = "00:00"
        
        return lb
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        switch indexPath.item {
        case 0:
            soundsCell  = collectionView.dequeueReusableCell(withReuseIdentifier:btnCellString , for: indexPath) as? EffectsBarsCell
            soundsCell?.workingInstance = self
            soundsCell?.imageBtn.image = iconsImages[indexPath.item]
 
            return soundsCell!
        case 1:
            effectsCell  = collectionView.dequeueReusableCell(withReuseIdentifier:btnCellString , for: indexPath) as? EffectsBarsCell
            effectsCell?.workingInstance = self
            effectsCell?.imageBtn.image = iconsImages[indexPath.item]
            return effectsCell!
        case 2 :
            textCell  = collectionView.dequeueReusableCell(withReuseIdentifier:btnCellString , for: indexPath) as? EffectsBarsCell
            textCell?.workingInstance = self
            textCell?.imageBtn.image = iconsImages[indexPath.item]
            print(view.frame.width)
            return textCell!
        default:
          break
        }
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier:btnCellString , for: indexPath)
        return cell
    }
    
  
    
    
  override func viewDidLoad() {
        super.viewDidLoad()
    navigationController?.navigationBar.isHidden = false
    navigationItem.hidesBackButton = true

        setupView()
    
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(300)) {
        
        self.videoPlayer.videoURL = self.url
        self.videoPlayer.setupPlayer()
    }
   
    let dedtime = DispatchTime.now() + .milliseconds(500)
    DispatchQueue.main.asyncAfter(deadline: dedtime) {
        self.playAction()
        
    }
    
    
    let backBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(dismissVc))
    backBtn.tintColor = .white
    navigationItem.leftBarButtonItem = backBtn
    
    let saveBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "exportIcone"), style: .plain, target: self, action: #selector(exportVideo))
    saveBtn.tintColor = .white
    navigationItem.rightBarButtonItem = saveBtn
    
    
    
  /*  let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(scale))
    let rotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(rotate))
    
    pinchGesture.delegate = self
    rotationGesture.delegate = self
    
    view.addGestureRecognizer(pinchGesture)
    view.addGestureRecognizer(rotationGesture)*/
    }
    
   
    @objc func dismissVc(){
        playAction()
       self.navigationController?.popToRootViewController(animated: true )
    }
  
    @objc func exportVideo(){
        playAction()

         let ex = MoveMakerVc()
        self.videoPlayer.player?.pause()

    print(videoPlayer.layer.frame)
        ex.setVideoInfo(url: self.url!, sounds: soundEffectsArray, stikers: effectArray)
        self.navigationController?.pushViewController(ex, animated: true )
        
    }
    
    
    
    
     func getVideoRect()->CGRect? {

        
        return (videoPlayer.palerlayer?.videoRect)
    }
    
    
     let layerView = UIView()
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
       self.playAction()
        if let videoSize = getVideoRect() {
            VideoInfoHelper.sheard.layerSize = videoSize
            layerView.backgroundColor = .clear
            layerView.frame = videoSize
            videoPlayer.addSubview(layerView )
        }
        
        switch indexPath.item  {
        case 0:
           let  controller = SoundsEffectsVc()
              controller.workinSpaceRef = self
            self.present(controller, animated: true, completion: nil)
        case 1 :
           let  controller = EffectsCategories()
           controller.workinSpaceRef = self
            self.present(controller, animated: true, completion: nil)
          

        case 2 :
          let   controller  = LablesEffectsVc()
         controller.workinSpaceRef = self
         self.present(controller, animated: true, completion: nil)
        default:
            break
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 75, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionBtns.frame.width, height:collectionBtns.frame.height / 3 )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return  5
    }
    
   
    
    
    func videoPlayerWihtUrl(url :URL)->VideoPlayer{
        
        let palyer = VideoPlayer(url:url , frame: view.frame)
        return palyer
    }
    
    
   
    
    
    func setupViewWidth(width:CGFloat , scaleValue : CGFloat){
        let value = width * scaleValue
        self.soundsCell?.viewSize = value
        self.effectsCell?.viewSize = value
        self.textCell?.viewSize = value
        
        self.soundsCell?.scaleVule = scaleValue
        self.textCell?.scaleVule = scaleValue
        self.effectsCell?.scaleVule = scaleValue
        self.videoPlayer.scaleValue = scaleValue
        isSetWidth = true
    }
    
    
    func relodCollections(){
        self.soundsCell?.effectsCollection.reloadData()
        self.effectsCell?.effectsCollection.reloadData()
        self.textCell?.effectsCollection.reloadData()
    }
    
    
    func scrollColletion(to PointX : CGFloat , PointY: CGFloat ){
        let point = CGPoint(x: PointX, y: PointY)
        DispatchQueue.main.async {
            self.soundsCell?.effectsCollection.setContentOffset(point, animated: true)
            self.effectsCell?.effectsCollection.setContentOffset(point, animated: true)
            self.textCell?.effectsCollection.setContentOffset(point, animated: true)
        }
      
    }
    var effectsLayer = CALayer()

    @objc func playAction(){
        if !isSetWidth {
        if let videoduration = self.videoPlayer.player?.currentItem?.duration {
            let secound = CMTimeGetSeconds(videoduration)
            setupViewWidth(width: CGFloat(secound), scaleValue: 30)
            
        }
            relodCollections()
        }
        
        DispatchQueue.global().async {
            self.effectsLayer.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
            self.effectsLayer.drawsAsynchronously = true
            DispatchQueue.main.async {
                self.videoPlayer.layer.addSublayer(self.effectsLayer)
            }

        }
        videoPlayer.onOffBtnFunction()
       
        
    }
    
    
    
    
    
    let topContaner: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let bottomContaner: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
 
    
    private func setupView(){
        
        view.addSubview(topContaner)
        _ = topContaner.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        topContaner.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5, constant: 60).isActive = true
       
    
       
        
        topContaner.addSubview(videoPlayer)
        videoPlayer.controllerReferanc = self
        videoPlayer.backgroundColor = .black
        let hight = self.view.frame.height * 0.5
        _ = videoPlayer.anchor(topContaner.topAnchor, left: topContaner.leftAnchor, bottom: nil, right: topContaner.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: hight)
    
   
        topContaner.addSubview(palyBtn)
        palyBtn.tintColor = .white
        _   =  palyBtn.anchor(nil, left: topContaner.leftAnchor, bottom: videoPlayer.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 75, heightConstant: 50)
        
        
        view.addSubview(bottomContaner)
        _ = bottomContaner.anchor(topContaner.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        let stackView = UIStackView(arrangedSubviews: [startScounds , remaingScounds])
    stackView.axis = .horizontal
    stackView.distribution = .fillEqually
    bottomContaner.addSubview(stackView)
   stackView.spacing = 10
    _  = stackView.anchor(videoPlayer.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
    stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
   
    collectionBtns.register(EffectsBarsCell.self, forCellWithReuseIdentifier:btnCellString )
        bottomContaner.addSubview(collectionBtns)

    _ = collectionBtns.anchor(stackView.bottomAnchor, left: bottomContaner.leftAnchor, bottom: bottomContaner.bottomAnchor, right: bottomContaner.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 30, rightConstant: 10, widthConstant: 0, heightConstant: 0)
         collectionBtns.showsVerticalScrollIndicator = false
    
    
         bottomContaner.addSubview(timeInspiterline)

          _ = timeInspiterline.anchor(bottomContaner.topAnchor, left: nil, bottom: collectionBtns.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 1, heightConstant: 0)
       timeInspiterline.centerXAnchor.constraint(equalTo: collectionBtns.centerXAnchor).isActive = true
    
    
   
}
    
    
    
  //  let hideBar = UIView()

    let gifview = GifEffectView()


    
    
   


}













extension workingSpaceVc {
    
    
    
    
    func addEffectFormUrl(url : URL){
        
        
        
        
      //  if let widow = UIApplication.shared.keyWindow {
        
           /* hideBar.backgroundColor = UIColor(white: 0, alpha: 0.5)

            widow.addSubview(self.hideBar)
            hideBar.frame  = CGRect(x: 0, y: 0, width: widow.frame.width, height: (self.navigationController?.navigationBar.frame.height)!)
            hideBar.alpha =  0*/
            

            
            gifview.setGifForView(Url: url )
            let effectController = StickersControllerView()
            effectController.workinSpaceInstance = self
            effectController.gifUrl = url
            effectController.giftView = gifview
            view.addSubview(effectController)
            effectController.frame = CGRect(x: 0, y: view.frame.height, width: view.frame.width, height: 0)
            
            gifview.frame = CGRect(x: self.layerView.frame.width / 2   , y: self.layerView.frame.height/2   , width: 0, height: 0 )
            
            UIView.animate(withDuration: 0.5, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
                effectController.frame = CGRect(x: 0, y: self.bottomContaner.frame.origin.y, width: self.bottomContaner.frame.width, height: self.bottomContaner.frame.height)
                
                self.layerView.addSubview(self.gifview)
                self.layerView.clipsToBounds = true 
                self.gifview.setParentFram(frame: self.layerView.frame)
            
               self.gifview.frame = CGRect(x: self.layerView.frame.width / 2  - 50 , y: self.layerView.frame.height/2  - 50 , width: 100, height: 134 )
             //   self.hideBar.alpha =  0

                
                
            }, completion: nil )
            
            
            
            
            

            
       // }
        
       
        
        
        
        
        
        
    }
    
    
    
 
    
    
    func editEffectForUrl(effectThumb : EfeectsThumbnails  ){
        
        gifview.setGifForView(Url: (effectThumb.efferct?.url)!)
        let effectController = StickerEditView()
        effectController.giftView = gifview
        effectController.workinSpaceInstance = self
        effectController.effectThumb = effectThumb
        
        effectController.barView = effectThumb.barView
      effectController.sliderValue.text = String(Int((effectThumb.efferct?.BeginTime)!)) + "  s"
        effectController.soundSlider.value = Float((effectThumb.efferct?.BeginTime)!)
        view.addSubview(effectController)
        effectController.frame = CGRect(x: 0, y: view.frame.height, width: view.frame.width, height: 0)
        
        gifview.frame = CGRect(x: self.layerView.frame.width / 2   , y: self.layerView.frame.height/2   , width: 0, height: 0 )
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            effectController.frame = CGRect(x: 0, y: self.bottomContaner.frame.origin.y, width: self.bottomContaner.frame.width, height: self.bottomContaner.frame.height)
            
            self.layerView.addSubview(self.gifview)
            self.gifview.frame = (effectThumb.efferct?.LayerFramInWorkspace)!
            
            
        }, completion: nil )
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    func addanimatedText(){
        
        
        
       
        
        
        
        
        
    }
    
    
    
    
    
    
    
    func addTextAnimationForId (text : String , textColor: UIColor , backroundColor: UIColor , animationId : String){
        
        
        let effectController = AddTextEffect()
        effectController.workinSpaceInstance = self
        view.addSubview(effectController)
        effectController.frame = CGRect(x: 0, y: view.frame.height, width: view.frame.width, height: 0)
        
        
        UIView.animate(withDuration: 0.5, delay: 0.2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            effectController.frame = CGRect(x: 0, y: self.bottomContaner.frame.origin.y, width: self.bottomContaner.frame.width, height: self.bottomContaner.frame.height)
            
            self.layerView.clipsToBounds = true
            
            
            
            
        }, completion: nil )
        
        
        let textEffectView = TextEffect()

        
        self.layerView.addSubview(textEffectView)
        textEffectView.frame = CGRect(x: 0, y: 0, width: 200, height: 50)
     
        let titleLayer = CATextLayer()
        // titleLayer.backgroundColor = UIColor.white.cgColor
        titleLayer.string = text
        titleLayer.shadowOpacity = 0
        titleLayer.fontSize = 25
        titleLayer.font = UIFont.getSemiBold(size: 10)
        titleLayer.alignmentMode = kCAAlignmentCenter
        titleLayer.frame = textEffectView.frame
        titleLayer.beginTime = 0
        titleLayer.foregroundColor = textColor.cgColor
        titleLayer.backgroundColor = backroundColor.cgColor
        titleLayer.isWrapped = true
        textEffectView.layer.addSublayer(titleLayer)
        setAnimationForText(id: animationId, Layer: titleLayer)
        
       
    }
    
    
    
    func setAnimationForText(id: String ,Layer : CATextLayer  ){
        switch id  {
        case "0":
            let textAnimation2 = CAKeyframeAnimation(keyPath: "transform.scale")
            textAnimation2.values =  [0,0.3, 0.6,1]
            textAnimation2.keyTimes =   [0.2 ,0.2,0.2,0.4]
            textAnimation2.repeatCount = 2
            textAnimation2.duration = 1
            Layer.add(textAnimation2, forKey: nil)

            
        case "1":
            let textAnimation = CAKeyframeAnimation(keyPath: "bounds.size.width")
            textAnimation.values =  [600 , 400, 200]
            textAnimation.keyTimes = [0.3 , 0.3 ,0.4]
            textAnimation.repeatCount = 7
            textAnimation.duration = 2
            
            textAnimation.isRemovedOnCompletion = false
            Layer.add(textAnimation, forKey: nil)
       
        case "2":
            let textAnimation = CAKeyframeAnimation(keyPath: "bounds.size.width")
            textAnimation.values =  [-400 , 0 , 200]
            textAnimation.keyTimes = [0.3 , 0.3 ,0.4]
            textAnimation.repeatCount = 7
            textAnimation.duration = 2
            
            textAnimation.isRemovedOnCompletion = false
            Layer.add(textAnimation, forKey: nil)
        case "3":
            let textAnimation = CAKeyframeAnimation(keyPath: "bounds.size.width")
            textAnimation.values =  [-600 , -400 , 200]
            textAnimation.keyTimes = [0.3 , 0.3 ,0.4]
            textAnimation.repeatCount = 2
            textAnimation.duration = 2
            
            textAnimation.isRemovedOnCompletion = false
            Layer.add(textAnimation, forKey: nil)
            let textAnimation2 = CAKeyframeAnimation(keyPath: "transform.scale")
            textAnimation2.values =  [0,0, 1.2,1]
            textAnimation2.keyTimes =   [0.2,0.1 ,0.3,0.4]
            textAnimation2.repeatCount = 2
            textAnimation2.duration = 2
            Layer.add(textAnimation2, forKey: nil)
            
        case "4":
            let textAnimation = CAKeyframeAnimation(keyPath: "bounds.size.width")
            textAnimation.values =  [600 , 400, 200]
            textAnimation.keyTimes = [0.3 , 0.3 ,0.4]
            textAnimation.repeatCount = 7
            textAnimation.duration = 2
            
            textAnimation.isRemovedOnCompletion = false
            Layer.add(textAnimation, forKey: nil)
            
            let textAnimation2 = CAKeyframeAnimation(keyPath: "transform.scale")
            textAnimation2.values =  [0,0, 1.2,1]
            textAnimation2.keyTimes =   [0.2,0.1 ,0.3,0.4]
            textAnimation2.repeatCount = 7
            textAnimation2.duration = 2
            Layer.add(textAnimation2, forKey: nil)
        case "5":
            let textAnimation = CAKeyframeAnimation(keyPath: "transform.scale.x")
            
            
            textAnimation.values = [0 , 1.29 , 1.15 , 1.1 ,1.2 ,0.5, 1 ]
            textAnimation.keyTimes = [0 , 0.3  , 0.35 , 0.35 ,0.3, 0.3,0.5]
            
            textAnimation.repeatCount = 1
            textAnimation.duration = 2
             Layer.add(textAnimation, forKey: nil)
            
            
        default:
            break
        }
        
        
    }
    
    
    
    
    
}
