//
//  EditText.swift
//  MyFillter
//
//  Created by Ahmad on 11/20/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit

class EditText: baseViewController ,UITextFieldDelegate {

    var controller : workingSpaceVc?
    var id : String?
    var textColor: UIColor?
    var backgroundColor: UIColor?
    private var colorCellId = "ColorCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false

        navigationItem.hidesBackButton = true
        let backBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backbtn))
        backBtn.tintColor = .white
        navigationItem.leftBarButtonItem = backBtn
        setupView()
        text.becomeFirstResponder()
        textColor = .white
        backgroundColor = .clear
    }
    

    @objc private func backbtn(){
        
        self.navigationController?.popViewController(animated: true )
    }

    
    
    lazy var  text:UITextField = {
        let txt = UITextField()
        txt.textAlignment = .center
        txt.font = UIFont.getSemiBold(size: 25)
        txt.attributedPlaceholder = NSAttributedString(string: "النص هنا", attributes: [.font:UIFont.getSemiBold(size: 15),.foregroundColor:UIColor.white])
        txt.tintColor = .white
        txt.textColor = .white
        txt.backgroundColor = .clear
        txt.delegate = self
        return txt
    }()
    
    
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true )
    }
    
    

    lazy var textColorCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        layout.scrollDirection = .horizontal
        collection.isPagingEnabled = true
      collection.showsHorizontalScrollIndicator = false
        collection.register(ColorCell.self, forCellWithReuseIdentifier: colorCellId)
        return collection
    }()
    
    lazy var backgroundColorCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        collection.isPagingEnabled = true
        layout.scrollDirection = .horizontal
        collection.showsHorizontalScrollIndicator = false

        collection.register(ColorCell.self, forCellWithReuseIdentifier: colorCellId)

        return collection
    }()
    
    let isClearbackground : UISwitch = {
        
        let swh = UISwitch()
        swh.isOn = true
        swh.tintColor = .white
        swh.addTarget(self, action: #selector(isClearbackgroundAction), for: .valueChanged)
        swh.onTintColor = .white
        return swh
    }()
    
    
    lazy var doneBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor =  UIColor.rgb(red: 40, green: 40, blue: 40)
        btn.titleLabel?.font = UIFont.getSemiBold(size: 20)
        btn.setTitle("متابعة", for:  .normal)
        btn.tintColor = .white
        btn.layer.cornerRadius = 25
      //  btn.addTarget(self, action: #selector(setText), for: .touchUpInside)
        return btn
    }()
    
    
    @objc func isClearbackgroundAction(){
        
        backgroundColorCollection.isHidden = !isClearbackground.isOn
        
        if !isClearbackground.isOn{
            text.backgroundColor = .clear
            self.backgroundColor = .clear
        }
    }
    
    @objc func setText(){
        self.navigationController?.popViewController(animated: true)
        self.controller?.addTextAnimationForId(text: self.text.text!, textColor: self.textColor!, backroundColor: self.backgroundColor!, animationId: id!)
        
    }
    
    private func setupView(){
        
        view.addSubview(text)
        _ = text.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 00, heightConstant: view.frame.height * 0.2)
        
        let colorLb = UILabel()
        colorLb.text = "لون النص :"
        colorLb.font = UIFont.getSemiBold(size: 18)
        colorLb.textColor = .white
      
        view.addSubview(colorLb)
        view.addSubview(textColorCollection)
        
      _ =   colorLb.anchor(text.bottomAnchor, left: nil, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 50)
        
        _ = textColorCollection.anchor(colorLb.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 110)
        
        
        let colorback = UILabel()
        colorback.text = "خلفية النص :"
        colorback.font = UIFont.getSemiBold(size: 18)
        colorback.textColor = .white
       
        
        view.addSubview(colorback)
        _ =   colorback.anchor(textColorCollection.bottomAnchor, left: nil, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 50)
        
        
        view.addSubview(backgroundColorCollection)
        _ = backgroundColorCollection.anchor(colorback.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 110)
        
        view.addSubview(doneBtn)
        _ = doneBtn.anchor(nil, left: nil, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 100, heightConstant: 50)
        doneBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        view.addSubview(isClearbackground)
        _ = isClearbackground.anchor(nil, left: nil, bottom: backgroundColorCollection.topAnchor, right:colorback.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 50, heightConstant: 50)

    }
    
    
    
    
 
    
    
    
    
    
    
    
}






extension  EditText : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 100
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: colorCellId, for: indexPath) as! ColorCell
        cell.color.backgroundColor = UIColor.random()
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == textColorCollection  {
            let cell = collectionView.cellForItem(at: indexPath) as! ColorCell
            text.textColor = cell.color.backgroundColor
            self.textColor = cell.color.backgroundColor
        }else {
            let cell = collectionView.cellForItem(at: indexPath) as! ColorCell
            text.backgroundColor = cell.color.backgroundColor
            self.backgroundColor = cell.color.backgroundColor
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}






