//
//  EffectsCategories.swift
//  MyFillter
//
//  Created by Ahmad on 11/21/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
class EffectsCategories:SoundsEffectsVc {
    
    var  Resorce = Bundle.main.paths(forResourcesOfType: nil, inDirectory: "Effect" )

    override func viewDidLoad() {
        super.viewDidLoad()
        titelImage.image = UIImage(named: "stickers")
        lableTitile.image = UIImage(named: "stickerslable")
      

    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: itemCellId, for: indexPath) as! soundItemCells
        
        let str  = Resorce[indexPath.item] as NSString
        let name  = str.lastPathComponent
        
        cell.itemName.text =  name
        cell.itemName.font = UIFont.getSemiBold(size: 15)
        cell.item.image = #imageLiteral(resourceName: "folder").withRenderingMode(.alwaysTemplate)
        cell.item.tintColor = UIColor.rgb(red: 237,green: 123,blue: 81)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return  Resorce.count
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let str  = Resorce[indexPath.item] as NSString
        let name  = str.lastPathComponent
        
            let stickers = StickersEffectsVc()
           stickers.pathName = name
            stickers.catcontrollr = self
            stickers.workinSpaceRef = self.workinSpaceRef
           self.present(stickers, animated: true) {
            }
    
     
        
        
    }
    
}
