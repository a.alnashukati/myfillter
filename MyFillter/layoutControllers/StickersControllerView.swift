//
//  StickersControllerView.swift
//  MyFillter
//
//  Created by Ahmad on 9/17/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit
class StickersControllerView  : soundEffectAddingView {
    
     var stikerController : AddSitckerEffectController?

    var giftView:GifEffectView?
    var gifUrl : URL?
    var effectObject : EffectsObject?
    var effectThumb = EfeectsThumbnails()

    override func setupView() {
        
        
      
        self.backgroundColor = UIColor.rgb(red: 40, green: 40, blue: 40)
        
        self.addSubview(sliderValue)
        
        self.addSubview(titleImage)
        
        addSubview(soundSlider)
        
        
        _ = sliderValue.anchor(self.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
      
        sliderValue.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true 
        _ = titleImage.anchor(nil, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 50, heightConstant: 50)
        
        titleImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true 
        
        _ = soundSlider.anchor(titleImage.topAnchor, left: titleImage.rightAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 50)
        
        let stackBtn = UIStackView(arrangedSubviews: [doneBtn ,removeBtn])
        stackBtn.axis = .horizontal
        stackBtn.distribution = .fillEqually
        self.addSubview(stackBtn)
        _ = stackBtn.anchor(nil, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 50, bottomConstant: 50, rightConstant: 50, widthConstant: 200, heightConstant: 0)
        
        titleImage.image = UIImage(named: "effectIcone")

    }
    
    override func doneAction() {
        
        
    let barView = workinSpaceInstance?.effectsCell?.cell?.mainViewContaner
        
        effectThumb.workinSpackRef = workinSpaceInstance
      effectThumb.barView = barView
        barView?.addSubview( effectThumb)
        
        let fullWidth = barView?.frame.width

        
        let  thumbnailsWidht : Int = 30
        let sliderValue = Int(self.soundSlider.value)
        let thumbnailsX = Int(fullWidth!) * sliderValue / 10
        if CGFloat( thumbnailsX) < fullWidth! {
            effectThumb.frame = CGRect(x: CGFloat(thumbnailsX), y: (barView?.frame.height)!/2 - 10 , width: CGFloat(thumbnailsWidht), height: 20)
        }else {
            
            effectThumb.frame = CGRect(x: thumbnailsX - thumbnailsWidht/2 , y: Int((barView?.frame.height)!/2 - 10), width: thumbnailsWidht, height: 20)
            
        }
        let s :CFTimeInterval = CFTimeInterval(Int(self.soundSlider.value))
        
        
         self.effectObject =  EffectsObject(With: (self.giftView?.frame)!, URL: self.gifUrl!, and: s)
            
        effectObject!.statPoint = Int(self.soundSlider.value)
        workinSpaceInstance?.effectArray.append(effectObject!)
        effectThumb.efferct = self.effectObject
        effectThumb.gifView = workinSpaceInstance?.gifview
          cancelView()
    }
    
   
    
    override func removeAction() {
cancelView()
    }
    
    
    
    func cancelView(){
        
     
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.frame = CGRect(x: 0, y: (self.workinSpaceInstance?.view.frame.height)!, width: (self.workinSpaceInstance?.view.frame.width)!, height: 0)
            
            
            self.giftView?.frame = CGRect(x: (self.workinSpaceInstance?.layerView.frame.width)! / 2   , y: (self.workinSpaceInstance?.layerView.frame.height)!/2   , width: 0, height: 0 )
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(450) , execute: {
                self.removeFromSuperview()

            })
            
            
        }) { (true ) in

        }
        
        
        
    }
    
    

    
}




class StickerEditView: StickersControllerView {
    
    
    var  barView : UIView?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func removeAction() {
        let index = self.workinSpaceInstance?.effectArray.lastIndex(of: self.effectThumb.efferct!)
        self.workinSpaceInstance?.effectArray.remove(at: index!)
        self.barView?.removeFromSuperview()
        cancelView()
    }
    
    
    
    override func doneAction() {
        
        effectThumb.workinSpackRef = workinSpaceInstance
        self.barView = effectThumb.barView
        barView?.addSubview( effectThumb)
        
        let fullWidth = barView?.frame.width
        
        
        let  thumbnailsWidht : Int = 30
        let sliderValue = Int(self.soundSlider.value)
        let thumbnailsX = Int(fullWidth!) * sliderValue / 10
        if CGFloat( thumbnailsX) < fullWidth! {
            effectThumb.frame = CGRect(x: CGFloat(thumbnailsX), y: (barView?.frame.height)!/2 - 10 , width: CGFloat(thumbnailsWidht), height: 20)
        }else {
            
            effectThumb.frame = CGRect(x: thumbnailsX - thumbnailsWidht/2 , y: Int((barView?.frame.height)!/2 - 10), width: thumbnailsWidht, height: 20)
            
        }
        let s :CFTimeInterval = CFTimeInterval(Int(self.soundSlider.value))
        
        
        self.effectObject = effectThumb.efferct
            //EffectsObject(With: (self.giftView?.frame)!, URL: self.gifUrl!, and: s)
          getObjectFromEffectArray().LayerFramInWorkspace = self.giftView?.frame
        getObjectFromEffectArray().BeginTime = s
        effectObject!.statPoint = Int(self.soundSlider.value)
      //  workinSpaceInstance?.effectArray.append(effectObject!)
        effectThumb.efferct = self.effectObject
        effectThumb.gifView = workinSpaceInstance?.gifview
        
        
        
        self.cancelView()
    }
    
    
    
    func getObjectFromEffectArray()->EffectsObject {
        let index =   self.workinSpaceInstance?.effectArray.lastIndex(of: effectThumb.efferct!)
        
        let  effect = self.workinSpaceInstance?.effectArray[index!]
        
        
        return effect!
    }
    
    
    
    override func cancelView(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            
            self.frame = CGRect(x: 0, y: (self.workinSpaceInstance?.view.frame.height)!, width: (self.workinSpaceInstance?.view.frame.width)!, height: 0)
            
            
            self.giftView?.frame = CGRect(x: (self.workinSpaceInstance?.layerView.frame.width)! / 2   , y: (self.workinSpaceInstance?.layerView.frame.height)!/2   , width: 0, height: 0 )
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(450) , execute: {
                self.removeFromSuperview()
                
            })
            
            
        }) { (true ) in
            
        }
        
        
    }
    
    
    
    
    
    
   
}
