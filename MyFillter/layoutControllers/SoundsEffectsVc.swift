//
//  SoundsEffectsVc.swift
//  MyFillter
//
//  Created by Ahmad on 6/30/18.
//  Copyright © 2018 Ahmad. All rights reserved.
//

import UIKit

class SoundsEffectsVc: ItemsCollection {
    var workinSpaceRef: workingSpaceVc?

    var soundArray = [SoundEffectsObject]()
    var soundsResource = Bundle.main.paths(forResourcesOfType: "mp3", inDirectory: nil)
    var soundsUrls = Bundle.main.urls(forResourcesWithExtension: "mp3", subdirectory: nil, localization: nil)

  private  let sound = Bundle.main.url(forResource: "3", withExtension: "mp3", subdirectory: nil)
    
    let titelImage : UIImageView = {
        let image = UIImageView(image: #imageLiteral(resourceName: "saounds"))
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    let lableTitile : UIImageView = {
        let image = UIImageView(image: #imageLiteral(resourceName: "soundlable"))
        image.contentMode = .scaleAspectFit
        
        return image
    }()
    
    lazy var  dismissBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        btn.tintColor = .white 
        btn.addTarget(self, action: #selector(btnAction), for: .touchUpInside)
        return btn
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal

    }

   
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return soundsResource.count
    }

    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: itemCellId, for: indexPath) as! soundItemCells
         let name  = "Sound" + " " + String(indexPath.item + 1 )
        cell.itemName.text =  name
  
        let url  = soundsUrls![indexPath.item]
        let item = SoundEffectsObject()
        item.url = url
        item.name = name
        soundArray.append(item)
        return cell

    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller  = AddEffectViewController()
        controller .modalPresentationStyle  = .overCurrentContext
        controller .workInctance = workinSpaceRef
        let view =  soundEffectAddingView()
        
        view.selectedSoundItem = soundArray[indexPath.item]
        view.playPuseBtn.setTitle(soundArray[indexPath.item].name, for: .normal)
        controller .contaner = view
        view.soundController = controller 
        view.workinSpaceInstance  = workinSpaceRef
       view.soundController = controller 
        
       
                self.dismiss(animated: true) {
                    self.workinSpaceRef?.present(controller , animated: true, completion: nil)
                }
        
        
        
    }
    
 
    
    @objc func btnAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 20, right: 10)
    }

    override func registerationCell() {
        itemsCollections.register(soundItemCells.self , forCellWithReuseIdentifier: self.itemCellId)

    }
    
    override func setupView() {
        
        view.addSubview(dismissBtn)
        view.addSubview(titelImage)
        view.addSubview(lableTitile)
        
        _ = titelImage.anchor(view.topAnchor, left: nil, bottom: nil, right: view.rightAnchor, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 75, heightConstant: 75)
       
        _ = lableTitile.anchor(view.topAnchor, left: nil, bottom: nil, right: titelImage.leftAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 100, heightConstant: 100)
      
        _ = dismissBtn.anchor(titelImage.topAnchor, left: view.leftAnchor, bottom: nil, right:nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 50 , heightConstant: 50)
        
        view.addSubview(itemsCollections)
        
        _ = itemsCollections.anchor(titelImage.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
   

}
