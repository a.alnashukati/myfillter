//
//  AuthService.swift
//  MyFillter
//
//  Created by Ahmad on 12/14/19.
//  Copyright © 2019 Ahmad. All rights reserved.
//

import Firebase
class AuthService {
    
    static   let inctance = AuthService()
    let ref = Database.database().reference()
  private  init() {
        
    }
    
    
    func getFeeds(completion : @escaping ([Feeds])->()){
        let feedsRef = ref.child(Databasekeys.public_videos.rawValue)
        
        feedsRef.observe(.value) { (snape) in
            if !snape.exists() {return}
            let value = snape.value  as? [String : Any ]
            var feedsArry = [Feeds]()
            for item in value! {
                let id = item.key
                let values = item.value as? [String : Any]
                let videoUrl = values![Databasekeys.url.rawValue] as? String
                let thum = values![Databasekeys.thumbnails.rawValue] as? String
                
                let feed = Feeds(id: id, videoUrl: videoUrl!, thumb: thum!)
                feed.setImage(image: self.getImageFromStr(str: thum!))
                feedsArry.append(feed)
            }
            
            completion(feedsArry)
            
        }
        
    }
    
    
    func getImageFromStr(str : String)->UIImage?{
        guard let url = URL(string: str) else {
            return nil
        }
        guard let ImageData = try? Data(contentsOf: url) else {return nil}
        
        let image = UIImage(data: ImageData)
        return image
    }
    
    enum Databasekeys: String {
        case public_videos , thumbnails ,url
    }
    
    
}
